<?php
/**
* Malik social network
* @author    Malik Umer Farooq
* @copyright 2017 Malik LIMITED
**/
// include lub
require_once "../includes/includes.php";
//check user login or not
$session->malik_is_checklogin();
$salts =  $_SESSION['login_user'];$session_user_id = malik_get_users_by_salts($salts)['id'];
$sth=$db->prepare("select * from users WHERE id=".$session_user_id);
$sth->execute();
$row = $sth->fetchObject();
$session_user_id =  $row->id;
$username = $row->username;
//Change User First And Last Name
if(isset($_POST['update_name'])){
    $fname = $_POST['fname'];
	$lname = $_POST['lname'];
	$create = $db->prepare("update users SET fname=?,lname=?where id=".$session_user_id);
	$create->execute(array(malik_escape($fname), malik_escape($lname)));
  malik_redirect("../profile.php?username=$username&pages=timeline&success=Name update successfully");
}
//Change User Email
if(isset($_POST['update_email'])){
    $email = $_POST['email'];
	$create = $db->prepare("update users SET email=?where id=".$session_user_id);
	$create->execute(array(malik_escape($email)));
  malik_redirect("../profile.php?username=$username&pages=timeline&success=Email update successfully");
}
//Change User password
if(isset($_POST['update_password'])){
    $password = $_POST['password'];
	$password_c = $_POST['password_c'];
	if($password ==$password_c){
			$hash = malik_ramdomstring_generator(30);
$password_conform = crypt($_POST["password"], $hash);
	$create = $db->prepare("update users SET password=?where id=".$session_user_id);
	$create->execute(array($password_conform));
  malik_redirect("../profile.php?username=$username&pages=timeline&success=Passowrd update successfully");

  }
}
//Change User bio
if(isset($_POST['update_bio'])){
    $bio = $_POST['bio'];
	$create = $db->prepare("update users SET bio=?where id=".$session_user_id);
	$create->execute(array(malik_escape($bio)));
  malik_redirect("../profile.php?username=$username&pages=timeline&success=Bio update successfully");

}
//Change User work
if(isset($_POST['update_work'])){
    $work = $_POST['work'];
	$create = $db->prepare("update users SET work=?where id=".$session_user_id);
	$create->execute(array(malik_escape($work)));
  malik_redirect("../profile.php?username=$username&pages=timeline&success=Work update successfully");

}

//Change User about
if(isset($_POST['update_about'])){
	$about = $_POST['about'];
	$create = $db->prepare("update users SET about=?where id=".$session_user_id);
	$create->execute(array(malik_escape($about)));
  malik_redirect("../profile.php?username=$username&pages=timeline&success=About update successfully");
}
//Change User Privacy
if(isset($_POST['p_submit'])){
$privacy = $_POST['privacy'];
$create = $db->prepare('UPDATE users SET privacy=?where id='.$session_user_id);
$create->execute(array(malik_escape($privacy)));
malik_redirect("../profile.php?username=$username&pages=timeline&success=privacy update successfully");
}
//Change User loc
if(isset($_POST['update_loc'])){
$privacy = $_POST['loc'];
$create = $db->prepare('UPDATE users SET loc=?where id='.$session_user_id);
$create->execute(array(malik_escape($privacy)));
malik_redirect("../profile.php?username=$username&pages=timeline&success=privacy update successfully");
}
//for links
if(isset($_POST['update_link'])){
	$facebook = $_POST["facebook"];
	$gmail = $_POST["gmail"];
	$youtube = $_POST["youtube"];
	$twitter = $_POST["twitter"];
	$insert = $db->prepare("update users set facebook=?,gmail=?,youtube=?,twitter=? where id='$session_user_id'");
	$insert->execute(array(
	malik_escape($facebook),malik_escape($gmail),malik_escape($youtube),malik_escape($twitter)));
	malik_redirect("../profile.php?username=$username&pages=timeline&success=links update successfully");
}
//for profile timeline block edit data form here 
	if(isset($_POST['editdata'])){
		$bio = $_POST['bio'];
		$name = $_POST['name'];
		$username = $_POST['username'];
		$work = $_POST['work'];
		$loc = $_POST['loc'];
		$about = $_POST['about'];
		$update = $db->prepare("Update users set bio=?,username=?,work=?,loc=?,about=?");
		$update->execute(array($bio,$username,$work,$loc,$about));
		malik_redirect("../profile.php?username=$username&pages=timeline&success=Data update successfully");
	}
?>
