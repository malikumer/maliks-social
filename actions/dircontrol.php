<?php
/**
* Malik social network
* @author    Malik Umer Farooq
* @copyright 2017 Malik LIMITED
**/
         $dir = './userdata/users/'.$session_user_id;
		$profiledir = 'profile';
		$coverdir =  'cover';
		$postdir  = 'post';
		$videos = 'video';
		$audios =  'audio';
		$files  = 'file';
		$back = "background";
		$page = 'pages';
		$subdirp = "./userdata/users/$session_user_id/".$profiledir;
		$subdirc = "./userdata/users/$session_user_id/".$coverdir;
		$subdirpp = "./userdata/users/$session_user_id/".$postdir;
		$subdirv = "./userdata/users/$session_user_id/".$videos;
		$subdira = "./userdata/users/$session_user_id/".$audios;
		$subdirf = "./userdata/users/$session_user_id/".$files;
		$background = "./userdata/users/$session_user_id/".$back;
		$pagesub = "./userdata/users/$session_user_id/".$page;
		if(!file_exists($dir)){
			mkdir($dir);
		}
		if(!file_exists($subdirp)){
			mkdir($subdirp);
		}
		if(!file_exists($subdirc)){
			mkdir($subdirc);
		}
		if(!file_exists($subdirpp)){
			mkdir($subdirpp);
		}
        if(!file_exists($subdirv)){
			mkdir($subdirv);
		}
		if(!file_exists($subdira)){
			mkdir($subdira);
		}
		if(!file_exists($subdirf)){
			mkdir($subdirf);
		}
		if(!file_exists($background)){
			mkdir($background);
		}
		if(!file_exists($pagesub)){
			mkdir($pagesub);
		}
?>