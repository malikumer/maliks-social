<?php
/**
* Malik social network
* @author    Malik Umer Farooq
* @copyright 2017 Malik LIMITED
**/
$folder = "./userdata/users/$page_owner/pages/";
$pname = $folder."$page_name/";
$image = $pname."image";
$audio = $pname."audio";
$video = $pname."video";
$file = $pname."file";
$profile = $pname."profile";
$cover = $pname."cover";
$background = $pname."background";
	if(!file_exists($folder)){
		mkdir($folder);
	}	
	if(!file_exists($pname)){
		mkdir($pname);
	}
	if(!file_exists($image)){
		mkdir($image);
	}	
	if(!file_exists($audio)){
		mkdir($audio);
	}
	if(!file_exists($video)){
		mkdir($video);
	}	
	if(!file_exists($file)){
		mkdir($file);
	}
		if(!file_exists($profile)){
		mkdir($profile);
	}
	if(!file_exists($cover)){
		mkdir($cover);
	}	
	if(!file_exists($background)){
		mkdir($background);
	}
?>	