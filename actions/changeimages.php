<?php
/**
* Malik social network
* @author    Malik Umer Farooq
* @copyright 2017 Malik LIMITED
**/
// include lib
require_once "../includes/includes.php";
//check user login or not
$session->malik_is_checklogin();
/**
* Malik social network
* @author    Malik Umer Farooq
* @copyright 2017 Malik LIMITED
**/
//profile picture
if(isset($_POST['submit'])){
	$file = $_FILES['file']['name'];
	$loc = $_FILES['file']['tmp_name'];
	$size = $_FILES['file']['size'];
	$file_ext = explode(".",$file);
 $file_new_ext = strtolower(end($file_ext));
 $newfile = malik_ramdomstring_generator(50).".".$file_new_ext;
 $salts =  $_SESSION['login_user'];
$session_user_id = malik_get_users_by_salts($salts)['id'];
$sth=$db->prepare("select * from users WHERE id=".$session_user_id);
   $sth->execute();
   $row = $sth->fetchObject();
   $session_user_id =  $row->id;
   $username = $row->username;
 $store ="../userdata/users/$session_user_id/profile/".$newfile;
	if($size >= 33554432){
		malik_redirect("../profile.php?username=$username&pages=timeline&error=Sorry the file size much be 4MB or less");
	}
	if($file_new_ext != "jpg" && $file_new_ext != "png" && $file_new_ext != "jpeg"
&& $file_new_ext != "gif" ) {
	malik_redirect("../profile.php?username=$username&pages=timeline&error=Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
}
	elseif(move_uploaded_file($loc,$store)){
		malik_redirect("../profile.php?username=$username&pages=timeline&success=Profile Picture Update successfully");
	}else{
		malik_redirect("../profile.php?username=$username&pages=timeline&error=Something went wrong!");
	}
	$create = $db->prepare("update users SET profile_picture=?where id=".$session_user_id);
	$create->execute(array(
    malik_escape($newfile))
    );
malik_redirect("../profile.php?username=$username&pages=timeline&success=Profile Picture update successfully!");
}
//cover picture
if(isset($_POST['submit1'])){
	$file = $_FILES['file1']['name'];
	$loc = $_FILES['file1']['tmp_name'];
	$size = $_FILES['file1']['size'];
	$file_ext = explode(".",$file);
 $file_new_ext = strtolower(end($file_ext));
 $newfile_c = malik_ramdomstring_generator(50).".".$file_new_ext;
 $sth=$db->prepare("select * from users WHERE id=".$_SESSION['login_user']);
   $sth->execute();
   $row = $sth->fetchObject();
   $session_user_id =  $row->id;
   $username = $row->username;
 $store ="../userdata/users/$session_user_id/cover/".$newfile_c;
	if($size >= 33554432){
       malik_redirect("../profile.php?username=$username&pages=timeline&error=Sorry,file size much be 4MB or less.");
	}
	if($file_new_ext != "jpg" && $file_new_ext != "png" && $file_new_ext != "jpeg"&& $file_new_ext != "gif" ) {
	     malik_redirect("../profile.php?username=$username&pages=timeline&error=Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
}
	elseif(move_uploaded_file($loc,$store)){
		malik_redirect("../profile.php?username=$username&pages=timeline&success=Profile Cover Update successfully");
	}else{
		malik_redirect("../profile.php?username=$username&pages=timeline&error=Someting went wrong!");
	}
	$create = $db->prepare("update users SET profile_cover=?where id=".$session_user_id);
	$create->execute(array(
    malik_escape($newfile_c))
    );
	malik_redirect("../profile.php?username=$username&pages=timeline&success=Profile Cover update successfully!");
}
//change background image of profile
if(isset($_POST['submit_design'])){
	$file = $_FILES['design']['name'];
	$loc = $_FILES['design']['tmp_name'];
	$size = $_FILES['design']['size'];
	$file_ext = explode(".",$file);
 $file_new_ext = strtolower(end($file_ext));
 $newfile_c = malik_ramdomstring_generator(50).".".$file_new_ext;
 $sth=$db->prepare("select * from users WHERE id=".$session_user_id);
   $sth->execute();
   $row = $sth->fetchObject();
   $session_user_id =  $row->id;
   $username = $row->username;
 $store ="../userdata/users/$session_user_id/background/".$newfile_c;
	if($size >= 33554432){
       malik_redirect("../profile.php?username=$username&pages=timeline&error=Sorry,file size much be 4MB or less.");
	}
	if($file_new_ext != "jpg" && $file_new_ext != "png" && $file_new_ext != "jpeg"&& $file_new_ext != "gif" ) {
	     malik_redirect("../profile.php?username=$username&pages=timeline&error=Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
}
	elseif(move_uploaded_file($loc,$store)){
		malik_redirect("../profile.php?username=$username&pages=timeline&success=Profile background update successfully!");
	}else{
		malik_redirect("../profile.php?username=$username&pages=timeline&error=Someting went wrong!");
	}
	$create = $db->prepare("update users SET background=?where id=".$session_user_id);
	$create->execute(array(
    malik_escape($newfile_c))
    );
	malik_redirect("../profile.php?username=$username&pages=timeline&success=Profile background update successfully!");
}
?>
