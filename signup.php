<?php 
include "includes/includes.php";
include "menu.php";
// it will never let you open index(login) page if session is set
             $cookies->malik_cookies_check();
             $session->malik_is_logged();
			 
?>
<?php
	$created = false; //of error report default

    $status = 'unactive'; // default value for register user
if(!isset($_POST['capcha']))
			{
				$_SESSION['capcha'] = malik_ramdomstring_generator(6);
			}
	if(!empty($_POST)){
		$errors = [];
		if(isset($_POST["username"])){
			if(strlen($_POST['username']) <= 4){
				$errors["username"] = "Username much be in five character";
			}elseif($_POST["username"] == 'admin'){
				$errors["username"] =  "This '" . $_POST["username"] . "' username not allowed by site owner";
			}
		}

		if(isset($_POST["username"])){
			if(!empty($_POST["username"]) && preg_match(('/^[a-zA-Z\-0-9_]+$/'), $_POST["username"])){
				$username = $db->quote($_POST["username"]);
				$select = $db->query("SELECT username FROM users WHERE username=$username");
				if($select->rowCount() >= 1){
					$errors["username"] = "username is already taken";
				}
			}else{
				$errors["username"] = "invalide username";
			}
		}
		if(isset($_POST["email"])){
			if (!empty($_POST["email"]) && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
				$email = $db->quote($_POST["email"]);
				$select = $db->query("SELECT email FROM users WHERE email=$email");
				if($select->rowCount() >= 1){
					$errors["email"] = "email  is already taken";
				}
			}else{
				$errors["email"] = "invalide email";
			}
		}

				if(isset($_POST["password"])){
			if(strlen($_POST['password']) <= 5){
				$errors["password"] = "password much be in six character";
			}
		}

		if(isset($_POST["password"])){
			if(!empty($_POST['password']) && $_POST["password"] != $_POST["confirm"]){
				$errors["password"] = "password doesn't match";
			}else if(empty($_POST["password"])){
				$errors["password"] = "invalide password";
			}
		}
           
			
			
			if(empty($_POST['capcha'])){
				$errors["capcha"] = "Capcha empty type character";
				$_SESSION['capcha'] = malik_ramdomstring_generator(6);
			}
			elseif($_SESSION['capcha'] !== $_POST['capcha']){
				$errors["capcha"] = "Capcha not match";
				$_SESSION['capcha'] = malik_ramdomstring_generator(6); 
				}
				
		if(empty($errors)){
			$create = $db->prepare("INSERT INTO users SET fname=?,lname=?,username=?, password=?,salts=? ,email=?,status=?,ip=?");
	        $hash = malik_ramdomstring_generator(30);
			$salts = malik_ramdomstring_generator(20);
            $password = crypt($_POST["password"], $hash);
           //providing protection form xss attack by escaping in to function
			$create->execute([malik_escape($_POST["fname"]),malik_escape($_POST["lname"]),malik_escape($_POST["username"]), malik_escape($password),malik_escape($salts),malik_escape($_POST['email']),malik_escape($status),$_POST['ip']]);
			$created = true;

		}
	}
	$username = @$_POST['username'];
   $email = @$_POST['email'];
   $subject = "$username please confirm your email address for Our website!";
   $message = "
Before you can start using our website, you must confirm your email address.
Please confirm your email address by clicking on the link below:
You may copy and paste the address to your browser manually in case the link does not work.
   ";
 mail($email,$subject,$message, 'form: noreply@url.com');
//show when success
	 require_once "alerts/error.php";
	 require_once "alerts/info.php";
	 require_once "alerts/success.php";
     require_once "forms/signup.php";
?>
