<?php
   	include "includes/includes.php";
   $session->malik_is_checklogin();
/**
* Malik social network
* @author    Malik Umer Farooq
* @copyright 2017 Malik LIMITED
**/ 
if(isset($_REQUEST['post'])){
	  $name = $_REQUEST['name'];
	  $p_id = $_REQUEST['id'];
      $content = $_REQUEST['content'];
      $loc_c = $_REQUEST['loc'];
      $file = $_FILES['img']['name'];
	  $loc = $_FILES['img']['tmp_name'];
      $size = $_FILES['img']['size'];
      $file_video = $_FILES['video']['name'];
	  $loc_video = $_FILES['video']['tmp_name'];
	  $size_video = $_FILES['video']['size'];
	  $file_audio = $_FILES['audio']['name'];
	  $loc_audio = $_FILES['audio']['tmp_name'];
	  $size_audio = $_FILES['audio']['size'];
	  $file_file = $_FILES['file']['name'];
	  $loc_file  = $_FILES['file']['tmp_name'];
	  $size_file  = $_FILES['file']['size'];
	  $data =  malik_replace_word($content);
	  $salts =  $_SESSION['login_user'];
	  $session_user_id = malik_get_users_by_salts($salts)['id'];
     $file_ext = explode(".",$file);
     $file_new_ext = strtolower(end($file_ext));
  	if(!empty($file)){
      $newfile = malik_ramdomstring_generator(50).".".$file_new_ext;
	 }else{
		$newfile = null;
	}
    $store ="userdata/users/$session_user_id/pages/$name/image/".$newfile;
 	if(!empty($file)){
	if($size >= 33554432){
		malik_redirect("profile.php?username=$username&pages=timeline&error=The image size much be 4MB or less");
	}
	if($file_new_ext != "jpg" && $file_new_ext != "png" && $file_new_ext != "jpeg"
     && $file_new_ext != "gif") {
	malik_redirect("profile.php?username=$username&pages=timeline&error=Sorry,in image only JPG, JPEG, PNG & GIF files are allowed.");
	die();
	}
	elseif(move_uploaded_file($loc,$store)){
	}else{
		malik_redirect("profile.php?username=$username&pages=timeline&error=Sorry, something went wrong");
	}
}
//for video
 $file_extv = explode(".",$file_video);
 $file_new_extv = strtolower(end($file_extv));
 if(!empty($file_video)){
 $newfile_v = malik_ramdomstring_generator(50).".".$file_new_extv;
 }else{
	 $newfile_v = null;
 }
 $store_videos ="userdata/users/$session_user_id/pages/$name/video/".$newfile_v;
 	if(!empty($file_video)){
	if($size_video >= 3353435454353462645432){
		malik_redirect("profile.php?username=$username&pages=timeline&error=The video file size much be 4MB or less");
	}
	if($file_new_extv != "mp4" && $file_new_extv != "3jp" && $file_new_extv != "wav"
&& $file_new_extv != "mpg") {
	malik_redirect("profile.php?username=$username&pages=timeline&error=Sorry, only mp4, 3jp, wav & mpg videos are allowed.");
	}
	elseif(move_uploaded_file($loc_video,$store_videos)){
		}
}
//for audio
	$file_exta = explode(".",$file_audio);
 $file_new_exta = strtolower(end($file_exta));
 if(!empty($file_audio)){
 $newfile_a = malik_ramdomstring_generator(50).".".$file_new_exta;
 }else{
	 $newfile_a = null;
 }
 $store_audio ="userdata/users/$session_user_id/pages/$name/audio/".$newfile_a;
 	if(!empty($file_audio)){
	if($size_audio >= 3353435454353462645432){
		malik_redirect("profile.php?username=$username&pages=timeline&error=Sorry, the audio file size much be 4MB or less");
	}
	if($file_new_exta != "mp3" && $file_new_exta != "wav" && $file_new_exta != "3jp") {
		malik_redirect("profile.php?username=$username&pages=timeline&error=Sorry, only  mp3, wav & 3jp audio files are allowed.");
	}	elseif(move_uploaded_file($loc_audio,$store_audio)){

		}else{
			malik_redirect("profile.php?username=$username&pages=timeline&error=Sorry, something went wrong");
		}
}
	/*  for files*/
	$file_extf = explode(".",$file_file);
 $file_new_extf = strtolower(end($file_extf));
 if(!empty($file_file)){
 $newfile_f = malik_ramdomstring_generator(50).".".$file_new_extf;
 }else{
	 $newfile_f = null;
 }
 $store_file ="userdata/users/$session_user_id/pages/$name/file/".$newfile_f;
 	if(!empty($file_file)){
	if($size_file >= 3353435454353462645432){
		malik_redirect("profile.php?username=$username&pages=timeline&error=Sorry, file size much be 4MB or less");
	}
	if($file_new_extf != "zip" && $file_new_extf != "exe" && $file_new_extf != "pdf") {
		malik_redirect("profile.php?username=$username&pages=timeline&error=Sorry, only zip, exe, pdf files are allowed.");
	}	elseif(move_uploaded_file($loc_file,$store_file)){
	}else{
		malik_redirect("profile.php?username=$username&pages=timeline&error=Sorry, something went wrong");
	}
}
  $time = time(); //getting current timeline
  $type = "page"; // set default type wall
	$create = $db->prepare("INSERT INTO post SET sender_id=?,sender_name=?,content=?,type=?,created=?,image=?,video=?,audio=?,file=?,loc=?,user_id=?");
	$create->execute(array(
    malik_escape($p_id),
    malik_escape($name),
    malik_escape($data),
    $type,
    $time,
    malik_escape($newfile),
    malik_escape($newfile_v),
    malik_escape($newfile_a),
    malik_escape($newfile_f),
    malik_escape($loc_c),
	$session_user_id
	));
	}
	malik_redirect("page?p=$p_id");
	
?>
