<?php
require_once "includes/includes.php";
if(isset($_POST["content_txt"]) && strlen($_POST["content_txt"])>0) 
	$post_id =  $_POST["post_id"];
    $user_id = $_POST['user_id'];
	$contentToSave = $_POST["content_txt"];  
	$salts =  $_SESSION['login_user'];
	$id = malik_get_users_by_salts($salts)['id'];
	$fname = malik_get_users_by_id($id)['fname'];
	$lname = malik_get_users_by_id($id)['lname'];
	$name = $fname . " " . $lname;
	 //$sender_name  = $_POST['sender_name'];
	 //$content  = $_POST['content'];
	 $time = time();
	$insert = $db->prepare("insert into post_comment set post_id=?,user_id=?,user_name=?,content=?,created=?");
	$insert->execute(
	array(
	$post_id,
	$user_id,
	$name,
	$contentToSave,
	$time
	)
	);
	if($insert)
	{
		 //Record was successfully inserted, respond result back to index page
		  echo " <div class='alert alert-success alert-dismissable'style='margin-top:-20px;'><i class='fa fa-check-circle' ></i>
		  Your comment posted successfully!
  <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    </div>";
	}else{
		
		//header('HTTP/1.1 500 '.mysql_error()); //display sql errors.. must not output sql errors in live mode.
		header('HTTP/1.1 500 Looks like mysql error, could not insert record!');
		exit();
	}
if(isset($_POST["recordToDelete"]))
{	
	$idToDelete = $_POST["recordToDelete"]; 
	$delete = $db->prepare("DELETE FROM post_comment where id=?");
	$delete->execute(array($idToDelete));
	
	if(!$delete)
	{    
		//If mysql delete query was unsuccessful, output error 
		header('HTTP/1.1 500 Could not delete record!');
		exit();
	}
}

?>
