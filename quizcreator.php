<?php
require_once "includes/includes.php";
require_once "menu.php";
require_once "alerts/success.php";
require_once "alerts/error.php";
require_once "alerts/info.php";
 ?>
<div class='row'><div class='col-md-12'>
<div class='col-md-8'>
<form action=''method='post'role='form'>
<div class='form-group'>
	<label for='nameofquiz'>Name of quiz.</label>
	<input type='text'name='nameofquiz'id='nameofquiz'class='form-control'>
</div>
<div class='form-group'>
	<label for='desc'>Name of quiz.</label>
	<textarea name='desc'id='desc'class='form-control'></textarea>
</div>
<!--
<div class='form-group'>
	<label for='q'>Question</label>
	<input type='text'name='q'id='q'class='form-control'>
	<label for='q1option1'>Option1</label>	
	<input type='text'name='q1option1'id='q1option1'class='form-control'>
	<label for='q1option2'>Option2</label>	
	<input type='text'name='q1option2'id='q1option2'class='form-control'>
	<label for='q1option3'>Option3</label>	
	<input type='text'name='q1option3'id='q1option3'class='form-control'>
	<label for='q1option4'>Option4</label>	
	<input type='text'name='q1option4'id='q1option4'class='form-control'>	
	<label for='q1answer'>Answer</label>	
	<input type='text'name='q1answer'id='q1answer'class='form-control'>	
</div>
-->
	<label for='whosee'>Who can see your quiz</label>
	<select class='form-control'id='whosee'name='whosee'>
		<option name='public'>Public</option>
		<option name='friends'>Friends</option>
		<option name='onlyme'>No(Onlyme)</option>
	</select>
	<label for='whojoin'>Who can access to join your quiz</label>
	<select class='form-control'id='whojoin'name='whojoin'>
		<option name='public'>Public</option>
		<option name='friends'>Friends</option>
		<option name='onlyme'>No(Onlyme)</option>
	</select><br>
		<input type='submit'class='btn btn-success'name='quizsubmit'value='submit'style='float:right;' />
</form></div>
		<div class='col-md-4'>
		<div id='showpost'>
		 <div class="panel panel-default">
			<div class='panel-heading'><h3  style='text-align:center'><?php echo $malik['ads:profile']; ?></h3></div></div>
			<?php 
			 require_once "Sponsors.php";
			?>
        </div>
		</div>
      </div>
    </div>
<?php 
if(isset($_POST['quizsubmit'])){
	 $quiz_name = $_POST['nameofquiz'];
	 $quiz_desc = $_POST['desc'];
	 $whosee = $_POST['whosee'];
	 $whojoin = $_POST['whojoin'];	
	 $insert = $db->prepare("INSERT INTO quiz SET q_name=?,q_desc=?,sender_id=?,sender_name=?,whosee=?,whojoin=?");
	 $insert->execute([
		 $quiz_name,
		 $quiz_desc,
		 malik_get_users_by_salts($_SESSION['login_user'])['id'],
		 malik_get_users_by_salts($_SESSION['login_user'])['fname']." ".  malik_get_users_by_salts($_SESSION['login_user'])['lname'],
		 $whosee,
		 $whojoin,
	 ]);
	 malik_redirect("quizcreator?success=Your quiz $quiz_name created successfully");
}
?>