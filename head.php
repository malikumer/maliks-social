<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="style/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="style/css/font-awesome.min.css">
<link rel='stylesheet'href='style/menu.css'>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="script/jquery/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="style/bootstrap/js/bootstrap.min.js"></script>
<link href="player/video-js.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="style/jquery.fancybox.min.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
<script src="script/jquery.fancybox.min.js"></script>
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
<script src="script/player/video.js"></script>
<script src="script/player/audiojs/audio.min.js"></script>
<script>
videojs.options.flash.swf = "player/video-js.swf";
</script>   
<?php ?>