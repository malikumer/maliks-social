<?php
//database function
function malik_database_connection(){
try{
	return $db = new PDO("mysql:host=localhost;dbname=lablnet_cms", "root" ,"");
	$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
}catch(Exception $e){
	die($e->getMessage());
}
}
//custom input field
function input($id, $type="text",$style=''){
return "<input type='$type' name='$id' id='$id'  style='$style'/>";
}
// custom redict
function malik_redirect($url){			 
	header("location:$url");
    /*echo "<script type='text/javascript'>
	window.location.href='$url'
	</script>";*/
}
function malik_is_friends($session_user_id,$user_id){
	$db = malik_database_connection();
	$select = $db->prepare("SELECT * FROM friends WHERE
	sender_id=$session_user_id AND reciver_id=$user_id OR
  sender_id=$user_id AND reciver_id=$session_user_id and status='pending'");
	$select->execute(array($session_user_id,$user_id));
	if($select->rowCount() <= 0){
	return true;
	}else{
	return false;
	}
}
function malik_is_friends_set($session_user_id){
	$db = malik_database_connection();
	$select = $db->query("SELECT * FROM friends Where sender_id=$session_user_id and status='pending'");
	if($select->rowCount() <= 0){
	 return $select->fetch();
	}else{
	return false;
	}
}
//random string for security
function malik_ramdomstring_generator($length) {
    $somestrings = '0123456789abcdefghijklmnFGSGSGFGSVHV EHDSHVHVSVHDVGFDopqfgsfsfsfsfrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $stringlength = strlen($somestrings);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $somestrings[rand(0, $stringlength - 1)];
    }
    return $randomString;
}
// function for replace profan word specified word
function malik_replace_word($SpecifiedWord){
	$ProfWord = array("fuck","sex","hate");
	$ReplaceWith = "___";
 $FinelWord = str_replace($ProfWord,$ReplaceWith,@$SpecifiedWord);
 return  $FinelWord;
}
/* rediction url passing */
function malik_root($value){
	return '/projects/social'."/".$value;
}
//function for excaping data avoid xss attack
function malik_escape($data){
	return htmlspecialchars($data);
}
//function for fetch webpage title
function malik_get_title_of_webpage($url){
	$str = file_get_contents($url);
	if(strlen($url) > 0 ){
		$str = trim(preg_replace('/\s+/',' ',$str));
		//adding line break if required
		preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title);
		return $title[1];
	}
}
//count total record in database
function malik_count_records($table){
	$db = malik_database_connection();
	$total = $db->prepare("Select * from $table");
	$total->execute();
	return $total->rowCount();
}
//site settings
function malik_site_settings($setting_name,$setting_value){
	$db = malik_database_connection();
	$insert = $db->prepare("update settings SET $setting_name=?");
	$insert->execute(array(
	$setting_value
	));
}
function malik_friend_update($sender_id,$reciver_id){
	$status = 'approvel';
	$db = malik_database_connection();
	$insert = $db->prepare("update settings SET status=? where sender_id=? and reciver_id=?");
	$insert->execute(array(
	$status,
	$sender_id,$reciver_id
	));
}
//get settung
function malik_get_setting_by_value (){
  $db = malik_database_connection();
  $users = $db->query("SELECT * FROM settings");
  if($users->rowCount() >= 1){
    return $users->fetch();
  }else {
    return array();
  }
}
function malik_visitors_counter($visited,$ip,$user_browser){
	$db = malik_database_connection();
	$insert = $db->prepare("Insert into visitors set visitor=?,ip=?,browser=?");
	$insert->execute(array(
	$visited,
	$ip,
	$user_browser
	));
}
function malik_friend($sender_id,$reciver_id){
	$db = malik_database_connection();
	$insert = $db->prepare("Insert into friends set sender_id=?,reciver_id=?");
	$insert->execute(array(
	$sender_id,
	$reciver_id
	));
}
function is_friend_request_send($reciver_id){
  $db = malik_database_connection();
  $select = $db->prepare("SELECT reciver_id FROM users WHERE reciver_id=$reciver_id");
  if ($select->rowCount() >= 1 ) {
    return true;
  }else{
    return false;
  }
}


?>
