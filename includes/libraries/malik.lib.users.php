<?php
//get users by id
function malik_get_users_by_id ($id){
  $db = malik_database_connection();
  $users = $db->prepare("SELECT * FROM users WHERE id=?");
  $users->execute(array($id));
  if($users->rowCount() >= 1){
    return $users->fetch();
  }else {
    return array();
  }
}
//get users by salts
function malik_get_users_by_salts ($salts){
  $db = malik_database_connection();
  $users = $db->prepare("SELECT * FROM users WHERE salts=?");
  $users->execute(array($salts));
  if($users->rowCount() >= 1){
    return $users->fetch();
  }else {
    return array();
  }
}
//get users by username
function malik_get_users_by_username ($username){
  	$db = malik_database_connection();
  $users = $db->prepare("SELECT * FROM users WHERE  username=?");
  $users->execute(array($username));
  if($users->rowCount() >= 1){
    return $users->fetch();
  }else {
    return array();
  }
}