<?php
//get post
function malik_get_post_by_id ($id){
  $db = malik_database_connection();
  $users = $db->query("SELECT * FROM post WHERE id=$id");
  if($users->rowCount() >= 1){
    return $users->fetch();
  }else {
    return array();
  }
}
//get post by sender id
function malik_get_post_by_sender_id ($id){
  $db = malik_database_connection();
  $users = $db->query("SELECT * FROM post WHERE sender_id=$id ORDER BY id DESC ");
  if($users->rowCount() >= 1){
    return $users->fetchAll(PDO::FETCH_ASSOC);
  }else {
    return array();
  }
}
//get post by user id and post id 
function malik_get_post_by_post_and_user_id($post_id,$user_id){
  $db = malik_database_connection();
  $posts = $db->prepare("SELECT * FROM post WHERE sender_id=$id And user_id=$user_id ");
  $posts->execute(array($post_id));
  if($users->rowCount() >= 1){
    return $users->fetchAll(PDO::FETCH_ASSOC);
  }else {
    return array();
  }
}
//delete post using id
function malik_delete_post_by_id ($id){
  $db = malik_database_connection();
  $delete = $db->prepare("DELETE FROM post where id=?");
  $delete->execute(array($id));
}
//edit post using id
function malik_edit_post_by_id ($content,$id){
  $db = malik_database_connection();
   $edit = $db->prepare("update post SET content=? where id=?");
$edit->execute(array($content,$id));
}