<?php
//get comment
function malik_get_comment_by_id ($id){
  $db = malik_database_connection();
  $users = $db->query("SELECT * FROM post_comment WHERE id=$id");
  if($users->rowCount() >= 1){
    return $users->fetch();
  }else {
    return array();
  }
}
//delete comment using id
function malik_delete_comment_by_id ($id){
  $db = malik_database_connection();
  $delete = $db->prepare("DELETE FROM post_comment where id=?");
  $delete->execute(array($id));
}
//edit comment using id
function malik_edit_comment_by_id ($content,$id){
  $db = malik_database_connection();
   $edit = $db->prepare("update post_comment SET content=? where id=?");
$edit->execute(array($content,$id));
}