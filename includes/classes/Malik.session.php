<?php
 class Session{
	 
	     //statting session when object created
		function __construct(){
			 session_start();
		 }
		 //checking login true or false
		public function malik_is_logged_in(){
			return (isset($_SESSION['login_user'])) ? true : false;
	   }
	    //if user session not set redirect to login page
		public function malik_is_checklogin(){
		$login = malik_root('login.php');
		if(!$this->malik_is_logged_in()){
		malik_redirect($login);
	      }
	   }
		//if session is set or users are login always redict to account page if user go to login/signup page
	    public function malik_is_logged(){
		$account = malik_root('account.php');
		 if ($this->malik_is_logged_in()!="" ) {
		 malik_redirect($account);
			exit();
		   }
	    }

}

?>