<?php
include ('DbConnect.php');

class User extends DbConnect //The extends allows me to hook up my db connection to any class i want.
{
	public $user_info;
	protected $user_db;
	
	function __construct($id){
		$this->user_db = new DbConnect();
		$this->user_info = $this->user_db->db->query("SELECT * FROM users WHERE id=".$id)->fetch(PDO::FETCH_ASSOC); //storing all the user info in the $user_info property for easy access later.
		
	}
	
	function getAllUserPosts(){
		return $this->user_db->db->query("SELECT * FROM post WHERE sender_id=".$this->user_info['id'])->fetchAll(PDO::FETCH_ASSOC);
	}
	
	function setUserSessionData(){
		$_SESSION['user'] = $this->user_info;
	}
	
	
}



$user = new User(27); //30 = id im looking for.
/*

What happens now is that im creating a new User object and storing it in the $user variable and because its a new instance of the User class the __contructor function is called.

(
A  __construct method is just a function that will be run automatically with out the need to call it like other methods do.

for eg the getAllUserPosts method needs to be called $user->getAllUserPosts(); before  you will see the posts.

)

what then happens is that i query for the user linked to set id and i store all the user information linked to that id within the $user_info property. (Property is a fancy word for variable found within a class)


*/
//So if i want to see the user id i tell php go look in the $user(Object) there you will find a property that is an array. Find the value linked to the key 'id';
var_dump($user->user_info['id']);  

var_dump($user->user_info['fname']);

var_dump($user->user_info['lname']);

$user->setUserSessionData(); // Here im telling php in the $user(Object) theres a method(function) call that function. But all that function will now do for me is take the $user_info and store that whole array in a Session so now you will be able to access it in our whole project like below.

echo '<pre>',print_r($_SESSION,true),'</pre>';

var_dump($_SESSION['user']['id']);
var_dump($_SESSION['user']['fname']);
var_dump($_SESSION['user']['lname']);

$user_posts = $user->getAllUserPosts(); //This will bring back all the posts of that particular user

echo '<pre>',print_r($user_posts,true),'</pre>';
?>