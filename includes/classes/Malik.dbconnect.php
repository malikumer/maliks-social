<?php
class DbConnect{
	
	protected $db;
	
	function __construct(){
		try{
			$this->db = new PDO("mysql:host=localhost;dbname=lablnet_cms", "root" ,"");
			$this->db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
			$this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			
		}catch(Exception $e){
			die($e->getMessage());
		}
	}
}
?>