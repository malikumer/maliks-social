<?php
// including database connection
require_once "db_conn.php";
// including different function's
require_once "libraries/system.php";
require_once "libraries/malik.lib.users.php";
require_once "libraries/malik.lib.post.php";
require_once "libraries/malik.lib.comment.php";
// including different redictors variable
require_once "variables/redirect_v.php";
//includng class for logout
require_once "classes/Malik.logout.php";
//includng class for getting browser name,version and os name
require_once "classes/Malik.browserdetection.php";
//time class
require_once "classes/Malik.time.php";
//ip class
require_once "classes/Malik.ip.php";
//role class
require_once "classes/Malik.role.php";
//session class
require_once "classes/Malik.session.php";
//cookies class
require_once "classes/Malik.cookies.php";
//handler class
require_once "classes/Malik.handler.php";
//handler class
require_once "classes/Malik.setting_on_of.php";
//handler class
require_once "classes/Malik.security.php";
//json class
require_once "classes/Malik.json.php";
//includng class for session users AJson
require_once "classes/session_user.php";
//includng class for collect users data
require_once "classes/user_data.php";
//mobile class
require_once "classes/Malik.detect.php";
//mobile class
require_once "classes/site_setting.php";
//including objects
require_once "objects.php";
require_once "local/malik.en.php";
?>
