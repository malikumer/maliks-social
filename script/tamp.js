var input, select, result;
function convert() {
    input = document.getElementById("number");
    select = document.getElementById("type");
    result = document.getElementById("result");
    
    var option = select.selectedIndex;
    var number = input.value;
    switch (option) {
        case 0:
            number = ctf(number);
            break;
        case 1:
            number = ctk(number);
            break;
        case 2:
            number = ftc(number);
            break;
        case 3:
            number = ftk(number);
            break;
        case 4:
            number = ktc(number);
            break;
        case 5:
            number = ktf(number);
            break;
        default:
            break;
    }
    result.innerHTML = `<h1>${number}</h1>`
}

function ctf(value) {
    return (value * 1.8) + 32;
}

function ctk(value) {
    return value + 273.15;
}

function ftc(value) {
    return (value - 32) / (9 / 5);
}

function ftk(value) {
    return (value + 459.67) / (9 / 5);
}

function ktc(value) {
    return value - 273.15;
}

function ktf(value) {
    return (value * (9/5)) - 459.67;
}