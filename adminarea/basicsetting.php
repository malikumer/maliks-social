	<?php
	require_once "../includes/includes.php";
	/**
	 * Malik social network
	 * @author    Malik Umer Farooq
	 * @copyright 2017 Malik LIMITED
	 **/
	?>
	<!DOCTYPE html>
	<head>
	<title><?php echo $malik['title:adminbassicsetting']; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.min.css" >
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<link href="css/style-responsive.css" rel="stylesheet"/>
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<link rel="stylesheet" href="css/morris.css" type="text/css"/>
	<!-- calendar -->
	<link rel="stylesheet" href="css/monthly.css">
	<!-- //calendar -->
	<!-- //font-awesome icons -->
	<script src="js/jquery2.0.3.min.js"></script>
	<script src="js/raphael-min.js"></script>
	<script src="js/morris.js"></script>
	</head>
	<body>
	<!--header start-->
	<?php require_once "header.php";?>
	<!--header end-->
	<!--sidebar start-->
	<?php require_once "sidebar.php";?>
	<!--sidebar end-->

			<section id="main-content">
		<section class="wrapper"> 
				<div class="typo-agile">  
	<!-- info show when success -->
				<?php
				$success =  @$_GET['success'];
				if(!empty($success)){
				?>
			<div class='alert alert-success alert-dismissable'style='margin-top:-20px;'>
					<i class="fa fa-check-circle" ></i>
					<?php echo  $success; ?>
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			</div>    
				<?php } ?>	   
																			<header class="panel-heading">
																				<?php echo $malik[							'title:adminbassicsetting'								]; ?>
																			</header>
							<br />
<div class="panel panel-default"id='postfooter'>
			  <div class="panel-heading">
			<?php echo $malik['heading1:adminbassicsetting'];?>
  </div>
</div>
<form action=''method='post'>
<div class="form-group">
  <label for="usr"><?php echo $malik['name:adminbassicsetting'];?></label>
  <input type="text" class="form-control" id="usr"name='name'required value="<?php echo malik_get_setting_by_value()['site_name']; ?>" >
</div>
<div class="form-group">
  <label for="desc"><?php echo $malik['desc:adminbassicsetting'];?></label>
  <textarea class="form-control" rows="5" id="desc"name='desc'><?php echo malik_get_setting_by_value()['site_description']; ?></textarea>
</div>
<div class="form-group">
  <label for="keyword"><?php echo $malik['keyword:adminbassicsetting'];?></label>
  <textarea class="form-control" rows="5" id="keyword"name='keyword'><?php echo malik_get_setting_by_value()['site_keywords']; ?><</textarea>
</div>
<input type='submit'name='submit'class="btn btn-primary btn-block"value='<?php echo $malik['button:adminbassicsetting'];?>'>
</form>
			<?php
              if(isset($_POST['submit'])){
				  $name = $_POST['name'];
				  $desc = $_POST['desc'];
				  $keyword = $_POST['keyword'];
				
				  malik_site_settings('site_name',$name);
				  malik_site_settings('site_description',$desc);
				  malik_site_settings('site_keyword',$keyword);
				  malik_redirect("basicsetting?success=setting save successfully");
			  }
			?>			
	<!-- footer --><br/>
				<div class="footer">
				<div class="malik-copyright">
					<?php
					//footer
					?>
				</div>
				</div> </div>
		<!-- / footer -->
	</section>
	<!--main content end-->
	</section>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/jquery.slimscroll.js"></script>
	<script src="js/jquery.nicescroll.js"></script>
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
	<script src="js/jquery.scrollTo.js"></script>
	<!-- morris JavaScript -->	

	</body>
	</html>