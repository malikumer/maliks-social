<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class="active" href="index">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                   <li>
                    <a class="active" href="users">
                        <i class="fa fa-users"></i>
                        <span>Users</span>
                    </a>
                </li>             
     
  
     
                <li>
                    <a href="post">
                        <i class="fa fa-square"></i>
                        <span>Posts </span>
                    </a>
                </li>
                <li>
                    <a href="comment">
                        <i class="fa fa-comment"></i>
                        <span>Comments</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-cogs"></i>
                        <span>Site settings</span>
                    </a>
                    <ul class="sub">
                        <li><a href="basicsetting">Basic</a></li>
                        <li><a href="capcha">Capcha</a></li>
						<li><a href="colors">Colors</a></li>
				        <li><a href="email">Email</a></li>
                        <li><a href="lang">Language</a></li>
						<li><a href="status">status</a></li>
				        <li><a href="weather">Weather</a></li>
                        <li><a href="news">News</a></li>
						<li><a href="jokes">Jokes</a></li>						
                    </ul>
                </li>
				<li>
                    <a href="ads">
                        <i class="fa fa-buysellads"></i>
                        <span>Add ads</span>
                    </a>
                </li>
            </ul>
		</div>
        <!-- sidebar menu end-->
    </div>
</aside>