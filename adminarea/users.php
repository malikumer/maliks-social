	<?php
	require_once "../includes/includes.php";
	/**
	 * Malik social network
	 * @author    Malik Umer Farooq
	 * @copyright 2017 Malik LIMITED
	 **/
	?>
	<!DOCTYPE html>
	<head>
	<title><?php echo $malik['users:admin']; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.min.css" >
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<link href="css/style-responsive.css" rel="stylesheet"/>
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<link rel="stylesheet" href="css/morris.css" type="text/css"/>
	<!-- calendar -->
	<link rel="stylesheet" href="css/monthly.css">
	<!-- //calendar -->
	<!-- //font-awesome icons -->
	<script src="js/jquery2.0.3.min.js"></script>
	<script src="js/raphael-min.js"></script>
	<script src="js/morris.js"></script>
	</head>
	<body>
	<!--header start-->
	<?php require_once "header.php";?>
	<!--header end-->
	<!--sidebar start-->
	<?php require_once "sidebar.php";?>
	<!--sidebar end-->

			<section id="main-content">
		<section class="wrapper"> 
				<div class="typo-agile">  
	<!-- info show when success -->
				<?php
				$success =  @$_GET['success'];
				if(!empty($success)){
				?>
			<div class='alert alert-success alert-dismissable'style='margin-top:-20px;'>
					<i class="fa fa-check-circle" ></i>
					<?php echo  $success; ?>
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			</div>    
				<?php } ?>	   
													<header class="panel-heading">
														<?php echo $malik['users:admin']; ?>
													</header>
							<br />
	<form action=''method='post'>					
	<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><?php echo $malik['adduser:adminuser']; ?></button>
	<input type="submit" class="btn btn-primary btn-lg" name='json'value='<?php echo $malik['json:adminuser']; ?>'/>
	<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mail"><?php echo $malik['mail:adminuser']; ?></button>
	<input type="submit" name='exel'class="btn btn-primary btn-lg" value='<?php echo $malik['cnv:adminuser']; ?>'/>
	</form><br />
	<form action=''method='post'>
	<div class="form-group">
		<input type="text"  placeholder='<?php echo $malik['search:adminview']; ?>' class="form-control input-lg" name='search'value='<?php echo $malik['search:adminview']; ?>' />
	</div>
	</form>
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><?php echo $malik['adduser:adminuser']; ?></h4>
				</div>
				<div class="modal-body">
						<form action=''method='post'>
					<div class="form-group">
						<label for="fname"><?php echo $malik['fname:adminuser']; ?></label>
						<input type="text" class="form-control" id="fname"name='fname'>
					</div>		   
					<div class="form-group">
						<label for="lname"><?php echo $malik['lname:adminuser']; ?></label>
						<input type="text" class="form-control" id="lname"name='lname'>
					</div>
					<div class="form-group">
						<label for="email"><?php echo $malik['email:adminuser']; ?></label>
						<input type="text" class="form-control" id="email"name='email'>
					</div>
					<div class="form-group">
						<label for="username"><?php echo $malik['username:adminuser']; ?></label>
						<input type="text" class="form-control" id="username"name='username'>
					</div>						
					<div class="form-group">
						<label for="password"><?php echo $malik['password:adminuser']; ?></label>
						<input type="password" class="form-control" id="password"name='password'>
					</div>
					<div class="form-group">
						<label for="type"><?php echo $malik['type:adminuser']; ?></label>
							<select class="form-control" id="type"name='user_type'>
							<option  name='admistrator'><?php echo $malik['admin:adminuser']; ?></option>
							<option name='modrator'><?php echo $malik['mode:adminuser']; ?></option>
							<option name='normal'><?php echo $malik['normal:adminuser']; ?></option>
							</select>
					</div>	

				</div>
				<div class="modal-footer">
				<input type='submit'name='submit'class='btn btn-primary'value='<?php echo $malik['submit:adminuser']; ?>'/>
			<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $malik['close:adminuser']; ?></button>
		</form>		
				</div>
			</div>

		</div>
	</div>
	<!-- Modal for send mail-->
<div id="mail" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $malik['mailtitle:adminview']; ?></h4>
      </div>
      <div class="modal-body">
          <form action=''method='post'>
			<div class="form-group">
			  <label for="mail"><?php echo $malik['mailsubject:adminview'];?></label>
			  <input type="text" class="form-control" id="mail"name='mail'required>
			</div>
			<div class="form-group">
			  <label for="message"><?php echo $malik['mailcontent:adminview'];?></label>
			  <textarea class="form-control" rows="5" id="message"name='message'></textarea>
			</div>
      </div>
      <div class="modal-footer">
       <input type='submit'name='mail'class='btn btn-primary'value='<?php echo $malik['mailsend:adminview']; ?>'/>
	  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $malik['close:adminuser']; ?></button>
	</form>		
      </div>
    </div>

  </div>
</div>
	<?php 
	if(isset($_POST['submit'])){
		$fname = $_POST['fname'];
		$lname = $_POST['lname'];
		$username = $_POST['username'];
		$email = $_POST['email'];
		$type = $_POST['user_type'];	
		$status = "active";
						$create = $db->prepare("INSERT INTO users SET fname=?,lname=?,username=?, email=?, password=?,status=?,role=?");
						$hash = malik_ramdomstring_generator(30);
							$password = crypt($_POST["password"], $hash);
						//providing protection form xss attack by escaping in to function
				$create->execute([malik_escape($fname),malik_escape($lname),malik_escape($username), malik_escape($email) , malik_escape($password),malik_escape($status),malik_escape($type)]);
				malik_redirect("users?success=User added successfully");
	}
	if(isset($_POST['json'])){
		$sth=$db->prepare("select * from users ORDER BY id DESC ");
	$sth->execute();
	while($row = $sth->fetchObject())
	{ 
			$id=$row->id; 
			$fname = $row->fname;
			$lname = $row->lname;
			$username = $row->username;
			$email = $row->email;
			$password = $row->password;
			$profile_picture = $row->profile_picture;
			$profile_cover  = $row->profile_cover;	 
			$background = $row->background; 
			$about = $row->about;
			$bio = $row->bio;
			$work = $row->work;
			$status = $row->status;
			$created = $row->created;
					$privacy  = $row->privacy;
			$ban = $row->ban;
			$facebook = $row->facebook;
			$gmail = $row->gmail;
			$youtube = $row->youtube;
			$twitter = $row->twitter;
			$point = $row->point;
	$records[] = array(
				'id'=> $id, 
				'First name' => $fname,
				'Last name' => $lname,
				'username' => $username,
				'email' => $email,
				'password' => $password,
				'profile_picture' => $profile_picture,
				'profile_cover' => $profile_cover,
				'background' => $background,
				'about' => $about,
				'bio' => $bio,
				'work' => $work,
				'status' => $status,
				'created' => $created,
				'privacy' => $privacy,
				'ban' => $ban,
				'facebook' => $facebook,
				'gmail' => $gmail,
				'youtube' => $youtube,
				'twitter' => $twitter,
				'point' => $point,
	
	);
	} 
	$response['users'] = $records;
	$fp = fopen('exportsfiles/users.json', 'w');
	fwrite($fp, json_encode($response , JSON_PRETTY_PRINT));
	malik_redirect("users?success=User table export in json successfully");
	fclose($fp);
	}
	if(isset($_POST['exel'])){
	$sth=$db->prepare("select * from users");
	$sth->execute();
	$fp = fopen('exportsfiles/users.csv', 'w');
	while( $row = $sth->fetch())
			{
					fputcsv($fp, $row);
			}
			malik_redirect("users?success=User table export in csv successfully");
			fclose($fp);
	}/*
if(isset($_POST['mail'])){
	$subject = $_POST['subject'];
	$message = $_POST['message'];
	$show  = $db->prepare("select * from users");
	$show->execute();
while($row = $show->fetchObject()){
	$email = $row->email;
	var_dump($email);
	die();
	$send_form = "orm: noreply@url.com";
	 mail($email,$subject,$message, $send_form);
	malik_redirect("users?success=mail send to  $name successfully");
}	
}	*/
	?>
				<div class="table-responsive">			
					<table class="table">
						<thead>
							<tr>
								<th><?php echo $malik['id:adminuser']; ?></th>		
								<th><?php echo $malik['name:adminuser']; ?></th>
								<th><?php echo $malik['username:adminuser']; ?></th>
								<th><?php echo $malik['email:adminuser']; ?></th>
								<th><?php echo $malik['view:adminuser']; ?></th>
							</tr>
						</thead>
						<tbody>
						<?php 
							$show  = $db->prepare("select * from users");
							$show->execute();
							while($row = $show->fetchObject()){
								$id = $row->id;
								$fname = $row->fname;
								$lname = $row->lname;
								$name = $fname . " " . $lname;
								$username = $row->username;	
								$email = $row->email;
							?>                    
							<tr>
							<?php if(!isset($_POST['search'])){?>
								<td><?php echo $id; ?></td>
								<td><?php echo $name; ?></td>
								<td><?php echo $username; ?></td>
								<td><?php echo $email; ?></td>
								<td><a href='usersview.php?id=<?php echo $id; ?>'><?php echo $malik['view:adminuser']; ?></a></td>
							</tr>
							<?php }} ?>				
							<tr>
							<?php if(isset($_POST['search'])){
								$search = $_POST['search'];
								$show  = $db->prepare("select * from users where fname='$search'");
								$show->execute();
								$row = $show->fetchObject();
									$id = $row->id;
									$fname = $row->fname;
									$lname = $row->lname;
									$name = $fname . " " . $lname;
									$username = $row->username;	
									$email = $row->email;

							?>
								<td><?php echo $id; ?></td>
								<td><?php echo $name; ?></td>
								<td><?php echo $username; ?></td>
								<td><?php echo $email; ?></td>
								<td><a href='usersview.php?id=<?php echo $id; ?>'><?php echo $malik['view:adminuser']; ?></a></td>
							</tr>
	<?php } ?>				
						</tbody>
					</table>
	</div>
	<!-- footer -->
				<div class="footer">
				<div class="malik-copyright">
					<?php
					//footer
					?>
				</div>
				</div> </div>
		<!-- / footer -->
	</section>
	<!--main content end-->
	</section>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/jquery.slimscroll.js"></script>
	<script src="js/jquery.nicescroll.js"></script>
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
	<script src="js/jquery.scrollTo.js"></script>
	<!-- morris JavaScript -->	

	</body>
	</html>
