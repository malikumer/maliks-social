	<?php
	require_once "../includes/includes.php";
	/**
	 * Malik social network
	 * @author    Malik Umer Farooq
	 * @copyright 2017 Malik LIMITED
	 **/
	?>
	<!DOCTYPE html>
	<head>
	<title><?php echo $malik['title:adminads']; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.min.css" >
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<link href="css/style-responsive.css" rel="stylesheet"/>
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<link rel="stylesheet" href="css/morris.css" type="text/css"/>
	<!-- calendar -->
	<link rel="stylesheet" href="css/monthly.css">
	<!-- //calendar -->
	<!-- //font-awesome icons -->
	<script src="js/jquery2.0.3.min.js"></script>
	<script src="js/raphael-min.js"></script>
	<script src="js/morris.js"></script>
	</head>
	<body>
	<!--header start-->
	<?php require_once "header.php";?>
	<!--header end-->
	<!--sidebar start-->
	<?php require_once "sidebar.php";?>
	<!--sidebar end-->

			<section id="main-content">
		<section class="wrapper"> 
				<div class="typo-agile">  
	<!-- info show when success -->
				<?php
				$success =  @$_GET['success'];
				if(!empty($success)){
				?>
			<div class='alert alert-success alert-dismissable'style='margin-top:-20px;'>
					<i class="fa fa-check-circle" ></i>
					<?php echo  $success; ?>
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			</div>    
				<?php } ?>	   
																			<header class="panel-heading">
				<?php echo $malik['heading:adminads']; ?>		
        	</header>
			<br />
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#ads"><?php echo $malik['add:adminads']; ?></button><br />
					
<!-- Modal -->
	<div id="ads" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><?php echo $malik['add:adminads']; ?></h4>
				</div>
				<div class="modal-body">
				<form action=''method='post'enctype="multipart/form-data">
					<div class="form-group">
						<label for="title"><?php echo $malik['ptitle:adminads']; ?></label>
						<input type="text" class="form-control" id="title"name='title'>
					</div>		   
					<div class="form-group">
						<label for="description"><?php echo $malik['pdesc:adminads']; ?></label>
						<input type="text" class="form-control" id="description"name='description'>
					</div>
					<div class="form-group">
						<label for="link"><?php echo $malik['plink:adminads']; ?></label>
						<input type="url" class="form-control" id="link"name='link'>
					</div>
					<div class="form-group">
						<label for="img"><?php echo $malik['pimg:adminads']; ?></label>
						<input type="file" class="form-control" id="img"name='img'>
					</div>						
				</div>
				<div class="modal-footer">
				<input type='submit'name='submit'class='btn btn-primary'value='<?php echo $malik['save:adminads']; ?>'/>
			<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $malik['close:adminuser']; ?></button>
		</form>		
				</div>
			</div>

		</div>
	</div>		
	<div class="table-responsive">			
					<table class="table">
						<thead>
							<tr>
								<th><?php echo $malik['ptitle:adminads']; ?></th>
								<th><?php echo $malik['pdesc:adminads']; ?></th>
								<th><?php echo $malik['plink:adminads']; ?></th>
								<th><?php echo $malik['delete:adminads']; ?></th>
							</tr>
						</thead>
						<tbody>		
							<tr>
							<?php 
								
								$show  = $db->prepare("select * from ads");
								$show->execute();
								while($row = $show->fetchObject()){
									$id = $row->id;
									$ad_title = $row->ad_title;
									$ad_desc = $row->ad_desc;
									$ad_img = $row->ad_img;	

							?>
								<td><?php echo $id; ?></td>
								<td><?php echo $ad_title; ?></td>
								<td><?php echo $ad_desc; ?></td>
								<td><a href='ads.php?delete=<?php echo $id; ?>&img=<?php echo $ad_img; ?>'><?php echo $malik['delete:adminads']; ?></a></td>
							</tr>
	<?php } ?>				
						</tbody>
					</table>
				</div>	
	<?php   
   if(isset($_REQUEST['submit'])){
	   $title = $_REQUEST['title']; 
	   $description = $_REQUEST['description'];
       $link = $_REQUEST['link'];
      $file = $_FILES['img']['name'];
	  $loc = $_FILES['img']['tmp_name'];
      $size = $_FILES['img']['size'];
      $file_ext = explode(".",$file);
      $file_new_ext = strtolower(end($file_ext));
  	  if(!empty($file)){
      $newfile = malik_ramdomstring_generator(50).".".$file_new_ext;
	 }else{
		$newfile = null;
	}
		$store ="adminads/".$newfile;
 	if(!empty($file)){
	if($size >= 33554432){
		malik_redirect("ads?success=The image size much be 4MB or less");
	}
	if($file_new_ext != "jpg" && $file_new_ext != "png" && $file_new_ext != "jpeg"
     && $file_new_ext != "gif") {
	malik_redirect("ads?success=Sorry,in image only JPG, JPEG, PNG & GIF files are allowed.");
	die();
	}
	elseif(move_uploaded_file($loc,$store)){
	}else{
		malik_redirect("ads?success=Sorry, something went wrong");
	}
}
	$insert = $db->prepare("INSERT INTO ads SET ad_title=?,ad_desc=?,ad_link=?,ad_img=?");
	$insert->execute(array(
	$title,$description,$link,$newfile
	));
         malik_redirect("ads?success=Ads submited successfully");		 
}
if(isset($_GET['delete'])){
	$delete_id = $_GET['delete'];
	$delete_img = $_GET['img'];
	$image_loc = "adminads/$delete_img";
	if(file_exists($image_loc)){
		unlink($image_loc);
	}
	$creates = $db->prepare("DELETE FROM ads where id=?");
	$creates->execute(array($delete_id));
	 malik_redirect("ads?success=Ads delete successfully");
}
?>
	<!-- footer --><br/>
	
				<div class="footer">
				<div class="malik-copyright">
					<?php
					//footer
					?>
				</div>
				</div> </div>
		<!-- / footer -->
	</section>
	<!--main content end-->
	</section>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/jquery.slimscroll.js"></script>
	<script src="js/jquery.nicescroll.js"></script>
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
	<script src="js/jquery.scrollTo.js"></script>
	<!-- morris JavaScript -->	

	</body>
	</html>