<?php
require_once "../includes/includes.php";
/**
 * Malik social network
 * @author    Malik Umer Farooq
 * @copyright 2017 Malik LIMITED
 **/
?>
	<!DOCTYPE html>
	<head>
	<title><?php echo $malik['title:adminnews']; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.min.css" >
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<link href="css/style-responsive.css" rel="stylesheet"/>
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<link rel="stylesheet" href="css/morris.css" type="text/css"/>
	<!-- calendar -->
	<link rel="stylesheet" href="css/monthly.css">
	<!-- //calendar -->
	<!-- //font-awesome icons -->
	<script src="js/jquery2.0.3.min.js"></script>
	<script src="js/raphael-min.js"></script>
	<script src="js/morris.js"></script>
	</head>
	<body>
	<!--header start-->
	<?php require_once "header.php";?>
	<!--header end-->
	<!--sidebar start-->
	<?php require_once "sidebar.php";?>
	<!--sidebar end-->

			<section id="main-content">
		<section class="wrapper"> 
				<div class="typo-agile">  
	<!-- info show when success -->
				<?php
				$success =  @$_GET['success'];
				if(!empty($success)){
				?>
			<div class='alert alert-success alert-dismissable'style='margin-top:-20px;'>
					<i class="fa fa-check-circle" ></i>
					<?php echo  $success; ?>
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			</div>    
				<?php } ?>	   
																			<header class="panel-heading">
																				<?php echo $malik[							'heading:adminnews'								]; ?>
																			</header>
							<br />
<div class="panel panel-default"id='postfooter'>
			  <div class="panel-heading">
			<?php echo $malik['heading1:adminnews'];?>
  </div>
</div>
<form action=''method='post'>
<div class="form-group">
 <div class="form-group">
  <label for="usr"><?php echo $malik['name:adminnews']." ".malik_get_setting_by_value()['weather']; ?></label>
	<select class="form-control"name='On'>
		<option name='On'><?php echo $malik['on:admin']; ?></option>
		<option name='Of'><?php echo $malik['off:admin']; ?></option>
	 </select>
</div>
</div>
<input type='submit'name='submit'class="btn btn-primary btn-block"value='<?php echo $malik['button:adminbassicsetting'];?>'>
</form>
			<?php
              if(isset($_POST['submit'])){
				  $val = $_POST['On'];			
				  malik_site_settings('news',$val);
				  malik_redirect("news?success=setting save successfully");
			  }
			?>			
	<!-- footer --><br/>
				<div class="footer">
				<div class="malik-copyright">
					<?php
					//footer
					?>
				</div>
				</div> </div>
		<!-- / footer -->
	</section>
	<!--main content end-->
	</section>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/jquery.slimscroll.js"></script>
	<script src="js/jquery.nicescroll.js"></script>
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
	<script src="js/jquery.scrollTo.js"></script>
	</body>
	</html>