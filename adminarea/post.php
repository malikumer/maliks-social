	<?php
	require_once "../includes/includes.php";
	/**
	 * Malik social network
	 * @author    Malik Umer Farooq
	 * @copyright 2017 Malik LIMITED
	 **/
	?>
	<!DOCTYPE html>
	<head>
	<title><?php echo $malik['title:adminpost']; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.min.css" >
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<link href="css/style-responsive.css" rel="stylesheet"/>
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<link rel="stylesheet" href="css/morris.css" type="text/css"/>
	<!-- calendar -->
	<link rel="stylesheet" href="css/monthly.css">
	<!-- //calendar -->
	<!-- //font-awesome icons -->
	<script src="js/jquery2.0.3.min.js"></script>
	<script src="js/raphael-min.js"></script>
	<script src="js/morris.js"></script>
	</head>
	<body>
	<!--header start-->
	<?php require_once "header.php";?>
	<!--header end-->
	<!--sidebar start-->
	<?php require_once "sidebar.php";?>
	<!--sidebar end-->

	<section id="main-content">
		<section class="wrapper"> 
				<div class="typo-agile">  
	<!-- info show when success -->
				<?php
				$success =  @$_GET['success'];
				if(!empty($success)){
				?>
			<div class='alert alert-success alert-dismissable'style='margin-top:-20px;'>
					<i class="fa fa-check-circle" ></i>
					<?php echo  $success; ?>
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			</div>    
				<?php } ?>	
				    <header class="panel-heading">
						<?php echo $malik['post:adminpost']; ?>
					</header>
            <div class="table-responsive">			
					<table class="table">
						<thead>
                            <tr>
								<th><?php echo $malik['id:adminpost']; ?></th>	
								<th><?php echo $malik['sender_name:adminpost']; ?></th>
								<th><?php echo $malik['content:adminpost']; ?></th>
							</tr>
						</thead>
						<tbody>      
                     <?php  
                     $show = $db->prepare("Select * from post ORDER BY id DESC ");
                     $show->execute();
                     while($row = $show->fetchObject()){
                       $id = $row->id;
                       $sender_name = $row->sender_name;
                       $content =$row->content;
                       ?>  
							<tr>
								<td><?php echo $id; ?></td>
								<td><?php echo $sender_name; ?></td>
								<td><?php echo $content; ?></td>
								<td><a href='viewpost?id=<?php echo $id; ?>'><?php echo $malik['view:adminuser']; ?></a></td>
                                <?php } ?>	

							</tr>
																		
						</tbody>
					</table>
                <!-- footer -->
                            <div class="footer">
                                <div class="malik-copyright">
                                            <?php
                                            //footer
                                            ?>
                                     </div>
                                 </div> 
                            </div>
                 </duv>
                    <!-- / footer -->
	</section>
	<!--main content end-->
	</section>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/jquery.slimscroll.js"></script>
	<script src="js/jquery.nicescroll.js"></script>
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
	<script src="js/jquery.scrollTo.js"></script>
	<!-- morris JavaScript -->	

	</body>
	</html>
