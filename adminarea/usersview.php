<?php
require_once "../includes/includes.php";
/**
 * Malik social network
 * @author    Malik Umer Farooq
 * @copyright 2017 Malik LIMITED
 **/
?>
<!DOCTYPE html>
<head>
<title><?php echo $malik['title:adminview']; ?></title>
<meta charset='UTF-8'>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link rel="stylesheet" href="css/bootstrap.min.css" >
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/style-responsive.css" rel="stylesheet"/>
<!-- font CSS -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- font-awesome icons -->
<link rel="stylesheet" href="css/font.css" type="text/css"/>
<link href="css/font-awesome.css" rel="stylesheet"> 
<link rel="stylesheet" href="css/morris.css" type="text/css"/>
<!-- calendar -->
<link rel="stylesheet" href="css/monthly.css">
<!-- //calendar -->
<!-- //font-awesome icons -->
<script src="js/jquery2.0.3.min.js"></script>
<script src="js/raphael-min.js"></script>
<script src="js/morris.js"></script>
</head>
<body>
<!--header start-->
<?php require_once "header.php";?>
<!--header end-->
<!--sidebar start-->
<?php require_once "sidebar.php";?>
<!--sidebar end-->

     <section id="main-content">
	 <section class="wrapper"> 
       <div class="typo-agile">     
                        <header class="panel-heading">
                           <?php echo $malik['heading:adminview']; ?>
                        </header>
												<br />
<?php
$id = $_GET['id'];
$show  = $db->prepare("select * from users where id='$id'");
$show->execute();
while($row = $show->fetchObject()){
	$id = $row->id;
	$fname = $row->fname;
	$lname = $row->lname;
	$name = $fname . " " . $lname;
	$username = $row->username;	
	$email = $row->email;
	$password = $row->password;
    $profile_picture = $row->profile_picture;
	$profile_cover = $row->profile_cover;
	$background = $row->background;
	$about = $row->about;
	$bio = $row->bio;
	$work = $row->work;
	$status = $row->status;
	$created = $row->created;
	$privacy = $row->privacy;
	$ban = $row->ban;
	$facebook = $row->facebook;
	$gmail = $row->gmail;
	$youtube = $row->youtube;
	$twitter = $row->twitter;
	$point = $row->point;
	$role = $row->role;
	$ip = $row->ip;

 ?>
<form action=''method='post'>					
<button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#edit"><?php echo $malik['edit:adminview']; ?></button>
<?php if($status === 'unactive'){ ?>
<input type="submit" class="btn btn-primary btn-lg" name='varified'value='<?php echo $malik['varified:adminview']; ?>'/>
<?php } ?>
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mail"><?php echo $malik['mail:adminview']; ?></button>
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#user_type"><?php echo $malik['type:adminview']; ?></button>
<input type="submit" class="btn btn-danger btn-lg" name='delete'value='<?php echo $malik['delete:adminview']; ?>'/>
<?php if($ban === 'yes'){ ?>
<input type="submit" name='unban'class="btn btn-primary btn-lg" value='<?php echo $malik['unban:adminview']; ?>'/>
<?php } ?>
<?php if($ban === null){ ?>
<input type="submit" name='ban'class="btn btn-warning btn-lg" value='<?php echo $malik['ban:adminview']; ?>'/>
<?php } ?>
</form>
 <br />
 
<h1><?php echo $id." ".$name; ?></h1>
<i><?php echo $malik['time:adminview']; ?> <?php echo  $time->malik_Time_Ago($created); ?></i><br />
<?php if(!empty($profile_picture)){?>
<img src='../userdata/users/<?php echo $id; ?>/profile/<?php echo $profile_picture; ?>'style='width:200px;height:200px;'/> 
<?php } else{?>
<img src='../userdata/users/default.jpg'style='width:200px;height:200px;'/> 

<?php } ?>
<div><b><?php echo $malik['username:adminview']; ?>  </b><?php echo $username; ?></div>
<div><b><?php echo $malik['email:adminview']; ?>  </b><?php echo $email; ?></div>
<div><b><?php echo $malik['password:adminview']; ?>  </b><?php echo $password; ?></div>
<div><b><?php echo $malik['status:adminview']; ?>  </b><?php echo $status; ?></div>
<div><b><?php echo $malik['about:adminview']; ?>  </b><?php echo $about; ?></div>
<div><b><?php echo $malik['bio:adminview']; ?>  </b><?php echo $bio; ?></div>
<div><b><?php echo $malik['work:adminview']; ?>  </b><?php echo $work; ?></div>
<div><b><?php echo $malik['privacy:adminview']; ?>  </b><?php echo $privacy; ?></div>
<div><b><?php echo $malik['bann:adminview']; ?>  </b><?php echo $ban; ?></div>
<div><b><?php echo $malik['facebook:adminview']; ?>  </b><?php echo $facebook; ?></div>
<div><b><?php echo $malik['gmail:adminview']; ?>  </b><?php echo $gmail; ?></div>
<div><b><?php echo $malik['youtube:adminview']; ?>  </b><?php echo $youtube; ?></div>
<div><b><?php echo $malik['twitter:adminview']; ?>  </b><?php echo $twitter; ?></div>
<div><b><?php echo $malik['point:adminview']; ?>  </b><?php echo $point; ?></div>
<div><b><?php echo $malik['role:adminview']; ?>  </b><?php echo $role; ?></div>
<div><b><?php echo $malik['ip:adminview']; ?>  </b><?php echo $ip; ?></div>
<?php } ?>

<!-- Modal for edit users-->
<div id="edit" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $malik['edit:adminview'] ." ". $name; ?></h4>
      </div>
      <div class="modal-body">
           <form action=''method='post'>
				<div class="form-group">
				  <label for="fname"><?php echo $malik['fname:adminuser']; ?></label>
				  <input type="text" class="form-control" id="fname"name='fname'value="<?php echo $fname; ?>">
				</div>		   
				<div class="form-group">
				  <label for="lname"><?php echo $malik['lname:adminuser']; ?></label>
				  <input type="text" class="form-control" id="lname"name='lname'value="<?php echo $lname; ?>">
				</div>
				<div class="form-group">
				  <label for="email"><?php echo $malik['email:adminuser']; ?>:</label>
				  <input type="text" class="form-control" id="email"name='email'value="<?php echo $email; ?>">
				</div>
				<div class="form-group">
				  <label for="username"><?php echo $malik['username:adminuser']; ?>:</label>
				  <input type="text" class="form-control" id="username"name='username'value="<?php echo $username; ?>">
				</div>						
				<div class="form-group">
				  <label for="password"><?php echo $malik['password:adminuser']; ?>:</label>
				  <input type="password" class="form-control" id="password"name='password'value="<?php echo $password; ?>">
				</div>
				<div class="form-group">
				  <label for="type"><?php echo $malik['type:adminuser']; ?>:</label>
					  <select class="form-control" id="type"name='user_type'>
						<option  name='admistrator'><?php echo $role; ?></option>
						<option  name='admistrator'><?php echo $malik['admin:adminuser']; ?></option>
						<option name='modrator'><?php echo $malik['mode:adminuser']; ?></option>
						<option name='normal'><?php echo $malik['normal:adminuser']; ?></option>
					  </select>
				</div>	

      </div>
      <div class="modal-footer">
       <input type='submit'name='submit'class='btn btn-primary'/>
	  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $malik['close:adminuser']; ?></button>
	</form>		
      </div>
    </div>

  </div>
</div>
<!-- Modal for user type-->
<div id="user_type" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $malik['type:adminview']; ?></h4>
      </div>
      <div class="modal-body">
           <form action=''method='post'>
				<div class="form-group">
				  <label for="type"><?php echo $malik['type:adminview']; ?></label>
				 <?php echo $malik['typehave:adminview']; ?>'<?php echo $role; ?>' <?php echo $malik['type:adminview']; ?>
					  <select class="form-control" id="type"name='user_type'>
						<option  name='admistrator'><?php echo $role; ?></option>
						<option  name='admistrator'><?php echo $malik['admin:adminuser']; ?></option>
						<option name='modrator'><?php echo $malik['mode:adminuser']; ?></option>
						<option name='normal'><?php echo $malik['normal:adminuser']; ?></option>
					  </select>
				</div>	

      </div>
      <div class="modal-footer">
       <input type='submit'name='type'class='btn btn-primary'value='<?php echo $malik['submit:adminuser']; ?>'/>
	  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $malik['close:adminuser']; ?></button>
	</form>		
      </div>
    </div>

  </div>
</div>
<!-- Modal for send mail-->
<div id="mail" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $malik['mailtitle:adminview']." ".$name; ?></h4>
      </div>
      <div class="modal-body">
          <form action=''method='post'>
			<div class="form-group">
			  <label for="mail"><?php echo $malik['mailsubject:adminview'];?></label>
			  <input type="text" class="form-control" id="mail"name='mail'required>
			</div>
			<div class="form-group">
			  <label for="message"><?php echo $malik['mailcontent:adminview'];?></label>
			  <textarea class="form-control" rows="5" id="message"name='message'></textarea>
			</div>
      </div>
      <div class="modal-footer">
       <input type='submit'name='mail'class='btn btn-primary'value='<?php echo $malik['mailsend:adminview']; ?>'/>
	  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $malik['close:adminuser']; ?></button>
	</form>		
      </div>
    </div>

  </div>
</div>
<?php 
if(isset($_POST['submit'])){
	$fname = $_POST['fname'];
	$lname = $_POST['lname'];
	$email = $_POST['email'];
	$username = $_POST['username'];
	$pass = $_POST['password'];
	$type = $_POST['user_type'];
	$update = $db->prepare("update users SET fname=?,lname=?,username=?,email=?,password=?,role=? where id=?");
	$hash = malik_ramdomstring_generator(30);
    $password = crypt($pass, $hash);
	$update->execute(array(
	$fname,
	$lname,
	$username,	
	$email,
	$password,
	$type,
	$id));
	malik_redirect("users?success=User $name updated successfully");
}
if(isset($_POST['type'])){
	$type = $_POST['user_type'];
	$typed = $db->prepare("update users SET role=? where id=?");
	$typed->execute(array($type,$id));
	malik_redirect("users?success=User $name role change successfully");
}
if(isset($_POST['varified'])){
	$actaviate = "active";
	$typed = $db->prepare("update users SET status=? where id=?");
	$typed->execute(array($actaviate,$id));
	malik_redirect("users?success=User $name account activatiate successfully");
}
if(isset($_POST['delete'])){
	$delete = $db->prepare("DELETE FROM users where id=?");
	$delete->execute(array($id));
	malik_redirect("users?success=User $name delete successfully");
}
if(isset($_POST['unban'])){
	$unban = null;
	$unbanned = $db->prepare("update users SET ban=? where id=?");
	$unbanned->execute(array($unban,$id));
	malik_redirect("users?success=User $name unban successfully");
}
if(isset($_POST['ban'])){
	$ban = "yes";
	$banned = $db->prepare("update users SET ban=? where id=?");
	$banned->execute(array($ban,$id));
	malik_redirect("users?success=User $name ban successfully");
}
if(isset($_POST['mail'])){
	$subject = $_POST['subject'];
	$message = $_POST['message'];
	$send_form = "orm: noreply@url.com";
	 mail($email,$subject,$message, $send_form);
	malik_redirect("users?success=mail send to  $name successfully");
}
?>
<br />
 <!-- footer -->
		  <div class="footer">
			<div class="malik-copyright">
			  <?php
			   //footer
			  ?>
			</div>
		  </div> </div>
  <!-- / footer -->
</section>
<!--main content end-->
</section>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scripts.js"></script>
<script src="js/jquery.slimscroll.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="js/jquery.scrollTo.js"></script>
<!-- morris JavaScript -->	

</body>
</html>

