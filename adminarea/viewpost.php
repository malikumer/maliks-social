<?php
require_once "../includes/includes.php";
/**
 * Malik social network
 * @author    Malik Umer Farooq
 * @copyright 2017 Malik LIMITED
 **/
?>
<!DOCTYPE html>
<head>
<title><?php echo $malik['title:adminpostview']; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link rel="stylesheet" href="css/bootstrap.min.css" >
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/style-responsive.css" rel="stylesheet"/>
<!-- font CSS -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- font-awesome icons -->
<link rel="stylesheet" href="css/font.css" type="text/css"/>
<link href="css/font-awesome.css" rel="stylesheet"> 
<link rel="stylesheet" href="css/morris.css" type="text/css"/>
<!-- calendar -->
<link rel="stylesheet" href="css/monthly.css">
<!-- //calendar -->
<!-- //font-awesome icons -->
<script src="js/jquery2.0.3.min.js"></script>
<script src="js/raphael-min.js"></script>
<script src="js/morris.js"></script>
</head>
<body>
<!--header start-->
<?php require_once "header.php";?>
<!--header end-->
<!--sidebar start-->
<?php require_once "sidebar.php";?>
<!--sidebar end-->

	<section id="main-content">
		<section class="wrapper"> 
				<div class="typo-agile">  
				                <header class='panel-heading'><?php echo $malik['heading:adminviewpost']; ?></header>
				<br />
				<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#edit"><?php echo $malik['edit:adminpostview']; ?></button>
				<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#delete"><?php echo $malik['delete:adminpostview']; ?></button>
<br /><br />
<?php
	        $id = $_GET['id'];
			$id =  malik_get_post_by_id($id)['id'];
			$sender_id = malik_get_post_by_id($id)['sender_id'];
			$sender_name =  malik_get_post_by_id($id)['sender_name'];
			$content =  malik_get_post_by_id($id)['content'];
			$created =  malik_get_post_by_id($id)['created']; 
			$image =  malik_get_post_by_id($id)['image'];
			$video =  malik_get_post_by_id($id)['video'];
			$audio =  malik_get_post_by_id($id)['audio'];
			$file =  malik_get_post_by_id($id)['file'];
			$privacy = malik_get_post_by_id($id)['privacy'];
			$type = malik_get_post_by_id($id)['type'];
			$loc = malik_get_post_by_id($id)['loc'];
			$share_name = malik_get_post_by_id($id)['share_name'];
			$hidden = malik_get_post_by_id($id)['hidden'];
			$final_time = $time->malik_Time_Ago($created);     
?>  
			<h1><?php echo $malik['pid:adminpostview']; ?><?php echo $id; ?></h1>
			<h2><?php echo $malik['uid:adminpostview']; ?><?php echo $sender_id; ?></h2><h2><?php echo $malik['sname:adminpostview']; ?><?php echo $sender_name; ?></h2>
			<?php if(!empty($privacy)){ ?>
               <b><?php echo $malik['privacy:adminpostview']; ?> <?php echo $privacy; ?></b>
	        <?php } ?>
			<?php if(!empty($hidden)){ ?>
               <b><?php echo $malik['hidden:adminpostview']; ?><?php echo $hidden; ?></b>
	        <?php } ?>						
			<?php if(!empty($type)){ ?>
               <b><?php echo $malik['type:adminpostview']; ?><?php echo $type; ?></b>
	        <?php } ?>
			<?php if(!empty($loc)){ ?>
               <i><?php echo $malik['loc:adminpostview']; ?><?php echo $loc; ?></i>
	        <?php } ?>
				<i><?php echo $malik['created:adminpostview']; ?><?php echo $final_time; ?></i>
				<p><?php echo $content; ?></p>
			<?php if(!empty($image)){ ?>
               <img src='../userdata/users/<?php echo $sender_id; ?>/post/<?php echo $image; ?>'style='width:200px;height:200px;'/>
	        <?php } ?><div class="clearfix"> </div>
			<?php if(!empty($video)){ ?>
               <video src='../userdata/users/<?php echo $sender_id; ?>/video/<?php echo $video; ?>'style='width:300px;height:300px;'/>
	        <?php } ?><div class="clearfix"> </div>
			<?php if(!empty($file)){ ?>
							<h1><a href='../userdata/users/<?php echo $sender_id; ?>/file/<?php echo $file; ?>'>Download file</a></h1>
	        <?php } ?>		<div class="clearfix"> </div>
			<?php if(!empty($audio)){ ?>
							<audio controls>
								<source  src='../userdata/users/<?php echo $sender_id; ?>/audio/<?php echo $audio; ?>' type="audio/ogg"style='width:200px;height:200px;'>
								Your browser does not support the audio element.
			             	</audio>
	        <?php } ?>	<div class="clearfix"> </div>
							<!-- Modal -->
				<div id="edit" class="modal fade" role="dialog">
				<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><?php echo $malik['edit:adminview'] ." ". $id; ?></h4>
							</div>
							<div class="modal-body">
								<form action=''method='post'>
									<textarea rows="6" cols="30"name='content'><?php echo $content; ?></textarea>				<input type='text'name='p_id'value='<?php echo $id; ?>'hidden>		
							</div>	
							<div class="modal-footer">
							<input type='submit'name='edit'class='btn btn-primary'/>
							<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $malik['close:adminuser']; ?></button>
							</form>		
							</div>
					</div>

				</div>
				</div>
								<!-- Modal -->
				<div id="delete" class="modal fade" role="dialog">
				<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><?php echo $malik['delete:adminview'] ." ". $id; ?></h4>
							</div>
							<div class="modal-body">
								<form action=''method='post'>
								   <?php echo $malik['deleteconform:adminpostview']; ?>		
								   <input type='text'name='p_delete'value='<?php echo $id; ?>'hidden>						
							</div>
							<div class="modal-footer">
							<input type='submit'name='delete'class='btn btn-primary'/>
							<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $malik['close:adminuser']; ?></button>
							</form>		
							</div>
					</div>

				</div>
				</div>
					<?php
						//edit post...
						if(isset($_POST['edit'])){
							$content = $_POST['content'];
							$id = $_POST['p_id'];
							malik_edit_post_by_id($content,$id);
								malik_redirect("post?success=Post $id edit successfully");
							
						}
						//post delete
						if(isset($_POST['delete'])){
							$id = $_POST['p_delete'];
							malik_delete_post_by_id($id);
								malik_redirect("post?success=Post $id delete successfully");
							
						}

					?>
<br />
                <!-- footer -->
					<div class="clearfix"> </div>
					<div class="clearfix"> </div>
                            <div class="footer">
                                <div class="malik-copyright">
                                            <?php
                                            //footer
                                            ?>
                                     </div>
                                 </div> 
                            </div>
                 </duv>
                    <!-- / footer -->
</section>
<!--main content end-->
</section>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scripts.js"></script>
<script src="js/jquery.slimscroll.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="js/jquery.scrollTo.js"></script>
<!-- morris JavaScript -->	

</body>
</html>
