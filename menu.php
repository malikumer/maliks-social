<?php
 include "head.php"; 
 include "includes/includes.php";
/**
* Malik social network
* @author    Malik Umer Farooq
* @copyright 2017 Malik LIMITED
**/
 ?>

    <nav class="navbar navbar-inverse navbar-static-top"style='background-color:black;color:white;border-radius: none;'>
    <div class="container-fluid">
     <div class="navbar-header">
      <span style="font-size:30px;cursor:pointer" onclick="openNav()"><?php echo $malik['name:menu']; ?></span>
    </div>
	<?php if($session->malik_is_logged_in() === true) {
		 $salts =  $_SESSION['login_user'];
	     $session_user_id = malik_get_users_by_salts($salts)['id'];
		?>
    <ul class="nav navbar-nav navbar-right" style='color:white;'>
      <li style='margin-top:-1.5px;'><a href="profile?username=<?php echo malik_get_users_by_id($session_user_id)['username'];?>&pages=timeline"><?php
	  $user_id = malik_get_users_by_id($session_user_id)['id'];
	  $user_profile =malik_get_users_by_id($session_user_id)['profile_picture'];
	  $username = malik_get_users_by_id($session_user_id)['username'];
	  if(!empty($user_profile)){
	  echo "<img src='userdata/users/$user_id/profile/$user_profile'style='width:20px;height:25px;' /> ";
	  echo  "<b style='color:white'>".malik_get_users_by_id($session_user_id)['fname']."</b>";}
	  else{
		  echo "<img src='userdata/users/default.jpg'style='width:20px;height:25px;' /> ";
	  echo  "<b style='color:white'>".malik_get_users_by_id($session_user_id)['fname']."</b>";
	  } ?></a></li>
      <li><a href="account"><b style='color:white'><?php echo $malik['home:menu']; ?> </b></a></li>
      <ul class="nav navbar-nav"style='border-radius: none'>
   	  <a href="#"><button type="button" class="btn btn-black btn-lg"style='background-color:black;color:black;'id="frd_Button"><span class="glyphicon glyphicon-user"style='color:white'><span class="badge"style='color:white;background-color:#5a84c9'></span></span></a>	  </button>
	  <div id="friend">
                    <h3><?php echo $malik['friend:menu']; ?></h3>
                    <div style="height:300px;color:black">
				<?php echo $malik['sfriend:menu']; ?>
					</div>
                    <div class="seeAll"><a href="#"><?php echo $malik['seeall:menu']; ?></a></div>
                </div>

   	  <a href="#">
   	  <button type="button" class="btn btn-black btn-lg"style='background-color:black;'id="msg_Button"><span class="glyphicon glyphicon-envelope"style='color:white'style='color:white'><span class="badge"style='color:white;background-color:#5a84c9'></span></span></a>
   	 </button> <div id="message">
                    <h3><?php echo $malik['message:menu']; ?></h3>
                    <div style="height:300px;color:black">
					<?php echo $malik['smessage:menu']; ?>.
					</div>
                    <div class="seeAll"><a href="#"><?php echo $malik['seeall:menu']; ?></a></div>
                </div>
	 <a href="#">
   	  <button type="button" class="btn btn-black btn-lg"style='background-color:black;'id="noti_Button"><span class="glyphicon glyphicon-globe"style='color:white'><span class="badge"style='color:white;background-color:#5a84c9'></span></span></a></button>
	   <div id="notifications">
                    <h3><?php echo $malik['notification:menu']; ?></h3>
                    <div style="height:300px;color:black">
					<?php echo $malik['snotification:menu']; ?>
					</div>
                    <div class="seeAll"><a href="#"><?php echo $malik['seeall:menu']; ?></a></div>
                </div>
       </ul>
	    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"style='background-color:black;color:white;margin-top:12px;'>
      <span class="caret"></span>
    </button>

    <ul class="dropdown-menu" role="menu">
	<?php if($role->malik_is_admin() === true){  ?>
       <li><a href='adminarea'><?php echo $malik['admin:menu']; ?></a></li>
	<?php }?>
		<?php if($role->malik_is_modrator() === true){  ?>
       <li><a href='adminarea'><?php echo $malik['mode:menu']; ?></a></li>
	<?php }?>
      <li><a href="profile.php?username=<?php echo malik_get_users_by_id($session_user_id)['username'];?>&pages=timeline"><?php
	  $user_profile = malik_get_users_by_id($session_user_id)['profile_picture'];
	  if(!empty($user_profile)){
	  echo "<img src='userdata/users/$user_id/profile/$user_profile'style='width:20px;height:25px;' /> ";
	  $fname = malik_get_users_by_id($session_user_id)['fname'];
	  $lname = malik_get_users_by_id($session_user_id)['lname'];
	  $name = $fname . " " . $lname;
	  echo  $name;}
	  else{
		    echo "<img src='userdata/users/default.jpg'style='width:20px;height:25px;' /> ";
  $name = malik_get_users_by_id($session_user_id)['fname'].malik_get_users_by_id($session_user_id)['lname'];
    echo  $name;
	  }
	  ?></a></li>   <li role="presentation" class="divider"></li>
	   <li><a href=""><?php echo $malik['message:menu']; ?></a></li>
	    <li><a href=""><?php echo $malik['friend:menu']; ?></a></li>
		 <li><a href=""><?php echo $malik['notification:menu']; ?></a></li>
		 <div class="panel panel-default">
			  <div class="panel-heading"style='margin-bottom:-15px;'>
			<?php echo $malik['page:menu']; ?>
					  </div>
					</div>
		 <?php 
		 //page script
		 $sth=$db->prepare("select * from pages where creator_id='$session_user_id'");
    		 $sth->execute();
    		while($row = $sth->fetchObject()){
				 $p_id = $row->id;
				 $p_name = $row->name;
			 ?>
			<li><a href="page?p=<?php echo $p_id; ?>"><?php echo $p_name; ?></a></li>
			<?php } //end page  ?>
			 <li><a href="pagecreator"><?php echo $malik['cpage:menu']; ?></a></li>
			<div class="panel panel-default">
		  <div class="panel-heading"style='margin-bottom:-15px;'>
			 <?php echo $malik['settings:menu']; ?>
				  </div>
				</div>
		 <li><a href='profile?username=<?php malik_get_users_by_id($session_user_id)['username'];?>&pages=pravicy'> <?php echo $malik['privacy:menu']; ?></a></li>
		 <li><a href='inviter'> <?php echo $malik['invite:menu']; ?></a></li>
	 <li><a href="save"> <?php echo $malik['savep:menu']; ?></a></li>
	<?php if($setting_on_of->malik_is_site_weather() === true){ ?> 
	 <li><a href="weather"> <?php echo $malik['weather:menu']; ?></a></li>
	<?php } ?>
	<?php if($setting_on_of->malik_is_site_tamp() === true){ ?> 
	 <li><a href="tamp"> <?php echo $malik['tamp:menu']; ?></a></li>
	<?php } ?>		
	<?php if($setting_on_of->malik_is_site_jokes() === true){ ?> 
	 <li><a href="jokes"> <?php echo $malik['jokes:menu']; ?></a></li>
	<?php } ?>
	<?php if($setting_on_of->malik_is_site_news() === true){ ?> 
	 <li><a href="news"> <?php echo $malik['news:menu']; ?></a></li>
	<?php } ?>	
	   <li><a href="profile?username=<?php malik_get_users_by_id($session_user_id)['username'];?>&pages=setting"> <?php echo $malik['setting:menu']; ?></a></li>
	   <li><a href="hidden"> <?php echo $malik['hidden:menu']; ?></a></li>
      <li><a href="logout"> <?php echo $malik['logout:menu']; ?></a></li>
    </ul>
  </div>
	  </ul>
	<?php }else{?>
		<ul class="nav navbar-nav navbar-right">
      <li><a href="signup"><span class="glyphicon glyphicon-user"></span>  <?php echo $malik['sign:menu']; ?></a></li>
      <li><a href="login"><span class="glyphicon glyphicon-log-in"></span>  <?php echo $malik['login:menu']; ?></a></li>
    </ul>
	<?php } ?>
  </div>
</nav>
<script src='script/menu.js'>
</script>
 