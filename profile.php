<?php 
/**
* Malik social network
* @author    Malik Umer Farooq
* @copyright 2017 Malik LIMITED
**/
		require_once "includes/includes.php"; 
		require_once "menu.php";
	     $session->malik_is_checklogin();
?>
 <link rel='stylesheet'href='style/profile.css'>
<link rel='stylesheet'href='style/post.css'>	
		<?php 
	         //getting session user data
		 $salts =  $_SESSION['login_user'];
	     $session_user_id = malik_get_users_by_salts($salts)['id'];
		 $username = $_GET['username'];		
	     $user_id =  malik_get_users_by_username($username)['id'];
		 $fname =  malik_get_users_by_username($username)['fname'];
		 $lname = malik_get_users_by_username($username)['lname'];
		 $name = $fname." ".$lname;
		 $username =   malik_get_users_by_username($username)['username'];
		 $email = malik_get_users_by_username($username)['email'];
		 $status = malik_get_users_by_username($username)['status'];
		 $about = malik_get_users_by_username($username)['about'];
		 $bio = malik_get_users_by_username($username)['bio'];
		 $work = malik_get_users_by_username($username)['work'];
		 $profile_picture =malik_get_users_by_username($username)['profile_picture'];
		 $profile_cover  =malik_get_users_by_username($username)['profile_cover'];
         $privacy  = malik_get_users_by_username($username)['privacy'];
		 $point = malik_get_users_by_username($username)['point'];
		 $country = malik_get_users_by_username($username)['loc'];
		 
		 //show message on error success info etc
	   require_once "alerts/error.php";
	   require_once "alerts/info.php";
	   require_once "alerts/success.php";
	 if(!empty($profile_cover)){
		?>		
	<div style=' background: linear-gradient(to right, #000000 25%,#ffcd02 25%, #ffcd02 50%, #e84f47 50%, #e84f47 75%, #65c1ac 75% , #a0c172 100%, #a0c172 100%);height:4px;margin-top:-20px'>.</div>
	<a href='photo.php?photoidc=<?php echo $profile_cover; ?>&useridc=<?php echo $user_id; ?>'><img src='userdata/users/<?php echo $user_id; ?>/cover/<?php echo  $profile_cover; ?>'style='height:200px;width:100%;max-width:100%;max-height:200px;top:0'/></a>
	<?php }
	 else{
			echo "
			<div style=' background: linear-gradient(to right, #000000 25%,#ffcd02 25%, #ffcd02 50%, #e84f47 50%, #e84f47 75%, #65c1ac 75% , #a0c172 100%, #a0c172 100%);height:4px;margin-top:-20px'>.</div>
			<img src='userdata/users/cover.png'
		    style='height:180px;width:100%;	max-width:100%;'/>";
	 }
	 if(!empty($profile_picture)){?>
			<a href='userdata/users/<?php echo $user_id ?>/profile/<?php echo  $profile_picture; ?>' data-fancybox data-caption="Profile Picture <?php echo $name; ?><br /><?php if($session_user_id === $user_id){ ?> <a data-fancybox data-src='#hidden-content-a' href='javascript:;' class='btn'>Change</a><?php } ?>" data-width="480" data-height="450">
			<img src='userdata/users/<?php echo $user_id ?>/profile/<?php echo  $profile_picture; ?>'style='
			height:130px;width:130px;
			max-width:200px; margin-top: -163px;margin-left:20px;border:2px solid white;'
		    style='@media all and (-webkit-min-device-pixel-ratio:0) and (min-resolution: .001dpcm) {  height:150px;
			width:130px;max-width:200px;
			margin-top: -300px;margin-left:20px;
			border:2px solid white;}'class="image"/>
			</a>
			  <div style="display: none;" id="hidden-content-a">
				<h1>Change your profile picture</h1>
				 <form action='actions/changeimages.php'method='post'enctype='multipart/form-data'>
					<input type='file' name='file'id='text'>
				<br />
				   <input type='submit' name='submit' id='submit' value='Upload'>
				</form>
			  </div>
			<?php } else{?>
		<img src='userdata/users/default.jpg'style='
			height:150px;width:130px;
			max-width:200px; margin-top: -170px;margin-left:20px;border:2px solid white;'
		    style='@media all and (-webkit-min-device-pixel-ratio:0) and (min-resolution: .001dpcm) {  height:150px;
			width:130px;max-width:200px;
			margin-top: -300px;margin-left:20px;
			 border:2px solid white;}'/>
			<?php	} ?>
			 <h1 id='user_full_name'><?php echo $name; ?>
			 		 <?php	if($status === 'active'){
			?>
			<i class="fa fa-check-square-o" aria-hidden="true"style='color:green'title='Varified'></i>
			<?php
		}if($status === 'unactive'){
			?>
			<i class="fa fa-times-circle-o" aria-hidden="true"style='color:red'title='unvarified'></i>
			<?php
		}
?>
			 </h1>
	
			<span id='user_point'><i class='fa fa-tasks' aria-hidden='true'> <b><?php echo $point; ?> </b></i></span>
			<?php
		// dir making script
		require_once "actions/dircontrol.php";
        echo "<title>$name</title>";
?>
<?php if(is_friend_request_send($user_id) !== true){ ?>
<form action=''method='post'>
<input type='submit'name='friend'hidden>
</form>
<?php }
if(isset($_POST['friend'])){
	malik_friend($session_user_id,$user_id);
} ?>
<?php		
		require_once "menus/profilemenu.php";
		?>
<style>
    body{
      background-image: url("userdata/users/<?php echo $user_id?>/background/<?php echo $users_data->Users(@background); ?>");
      background-size:100% 100%;
      background-repeat: no-repeat;
      background-attachment: fixed;
    }
</style>
<?php
	$pages = @$_GET['pages'];
	switch ($pages) {
		case "timeline":
		//cheking session user
		   if($session_user_id === $user_id){
			  ?>
			  <!-- post styleshet -->
<!-- post form -->
 <?php 
 require_once "forms/post.php";
 require_once "forms/profile.php";	
 }
 if($session_user_id !== $user_id){
	  require_once "forms/publicprofile.php";	
 }
 break;
		case "friends":
				require_once "friends.php";
			break;
		case "about":
				require_once "about.php";
			break;
		case "setting":
			if($session_user_id == $user_id){
				require_once "setting.php";	
			}
			break;
		case "chname":
			if($session_user_id == $user_id){
				require_once "setting.php";
				require_once "forms/changename.php";	
      }
			break;
			case "chinfo":
				if($session_user_id == $user_id){
				require_once "setting.php";
				require_once "forms/changeinfo.php";	
				}
				else{
					echo " <script>window.location.href='profile.php?id=$session_user_id'</script>";
				   }
				break;
					case "chpassword":
						if($session_user_id == $user_id){
		require_once "setting.php";
       require_once "forms/changepassword.php";	
						}
					  else{
							echo "            <script>window.location.href='profile.php?id=$session_user_id'</script>";
						   }
						break;

						case 'pravicy':
						if($session_user_id == $user_id){
		require_once "setting.php";
       require_once "forms/changeprivacy.php";	
       }
			 else{
			 echo "            <script>window.location.href='profile.php?id=$session_user_id'</script>";}
			break;

						case "chprofile":
							if($session_user_id == $user_id){
		require_once "setting.php";
       require_once "forms/changeprofileimage.php";	
							}
						  else{
								echo "            <script>window.location.href='profile.php?id=$session_user_id'</script>";
							   }
							   break;
			  case "chcover":
							if($session_user_id == $user_id){
		require_once "setting.php";
       require_once "forms/changecoverimage.php";	
							}
						  else{
								echo "            <script>window.location.href='profile.php?id=$session_user_id'</script>";
							   }
							break;
							case "chusername":
				if($session_user_id == $user_id){
		require_once "setting.php";
       require_once "forms/changeusername.php";	
				}
				else{
					echo "            <script>window.location.href='profile.php?id=$session_user_id'</script>";
				   }
				break;
							case "chbio":
				if($session_user_id == $user_id){
		require_once "setting.php";
       require_once "forms/changebio.php";	
				}
				else{
					echo "            <script>window.location.href='profile.php?id=$session_user_id'</script>";
				   }
				break;
				case "chwork":
				if($session_user_id == $user_id){
		require_once "setting.php";
       require_once "forms/changework.php";	
				}
				else{
					echo "            <script>window.location.href='profile.php?id=$session_user_id'</script>";
				   }
				break;

		case "chabout":
			if($session_user_id == $user_id){
			require_once "setting.php";
		   require_once "forms/changeabout.php";		
			}
			else{
				echo "            <script>window.location.href='profile.php?id=$session_user_id'</script>";
			   }
			break;
		case "chloc":
			if($session_user_id == $user_id){
			require_once "setting.php";
		   require_once "forms/changelocation.php";		
			}
			else{
				echo "            <script>window.location.href='profile.php?id=$session_user_id'</script>";
			   }
			break;	
      case "design":
      if($session_user_id == $user_id){
        require_once "setting.php";
       require_once "forms/design.php";
      }
      else{
        echo "            <script>window.location.href='profile.php?id=$session_user_id'</script>";
         }
      break;
      case "links":
      if($session_user_id == $user_id){
        require_once "setting.php";
        require_once "forms/links.php";
      }
      else{
        echo "            <script>window.location.href='profile.php?id=$session_user_id'</script>";
         }
      break;
			case 'delaccount':
						if($session_user_id == $user_id){

	require_once "setting.php";
       echo "   <h1>Delete your account now</h1>
	          <a data-fancybox data-src='#delete' href='javascript:;' class='btn'><button type='button' class='btn btn-primary'>Delete This Account</button></a></div>
			   </div>
	";?>
				  <div style="display: none;" id="delete">
				<h1>Are you sure delete account</h1>
				 <a href='delete.php?id=<?php echo $id; ?>'><button type='button' class='btn btn-primary' onclick='delete_account()'>Delete This Account</button></a>
			  </div>
	<?php }
			 else{
			      echo "            <script>window.location.href='profile.php?id=$session_user_id'</script>";}
			 break;
		default:
    malik_redirect("profile.php?username=$username&pages=timeline&error=No page found");

	}
	?>
<script src='script/scripts.js'></script>

