<link rel='stylesheet'href='style/countrer.css'>
<form action='post.php'method='post'enctype="multipart/form-data">
<div class="panel panel-default">
	<div class='panel-heading'style='width:100%;max-width:100%;'><i class="fa fa-sticky-note" aria-hidden="true"></i> Write a post!</div><div class="wrap">
		<textarea name='content'id="example1"maxlength='10000'> </textarea>
		<span id='count'style=' 
		 background-color: black;
			color: white;
			font-size: 17px;
			padding: 8px;
			line-height: 12px;
			height: 24px;
			text-align: center;
			font-weight: bold;
		}'></span>
	<script src='script/counter.js'></script>
 <!-- for tag -->
 	<div id="tag" hidden>
		<h1>Comming soon</h1>
	</div>
	 <!-- for location -->
	<div id="fn" hidden>
		<div id="locationField">
			<input id="autocomplete" placeholder="Enter your address"
             onFocus="geolocate()" type="text"name="loc"></input>
		</div> 
	</div> 
	<script>
	   //google map api
 var placeSearch, autocomplete;
   var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDS8nzOOzHt-wKlU5Eo__ZSeGjmbxIu3GU&libraries=places&callback=initAutocomplete"
         async defer></script>
 <!-- for image -->
	<div id="image" hidden>
		<input  type="file"name='img'multiple />
	</div>
	 <!-- for audio -->
	<div id="audio" hidden>
    	<input  type="file"name='audio'/>
	</div>
	 <!-- for video -->
	<div id="video" hidden>
		<input  type="file"name='video'/>
	</div>
	 <!-- for file -->
	<div id="file" hidden>
		<input  type="file"name='file'/>
	</div>
	 <!-- for privacy -->
	<div id="privacy" hidden>
		<div class='form-group'>
			<label for='sel1'>Select Option (select one):</label>
            <select class='form-control' id='sel1'name='privacy'>
				<option name='public'><?php echo malik_get_users_by_salts($salts)['privacy']; ?></option>
				<option name='public'>public</option>
				<option name='friends'>Friends	</option>
				<option name='onlyme'>Only Me</option>
            </select>
      </div>
	</div>
</div>
<div class="panel-footer"style='width:100%;max-width:100%;'>
<div class="icons">
      <!-- for tag -->
    <label>
		<i class="fa fa-users"style="font-size:24px;cursor:hand;color:rgba(0,0,0,0.8 );"id="tags"value="Click"></i>
    </label>
	 <!-- for picrure -->
    <label>
		<i class="fa fa-picture-o"style="font-size:24px;cursor:hand; padding-left:12px;color:rgba(0,0,0,0.8 );"id="images"value="Click"></i>
    </label>
	 <!-- for audio -->
	<label>
		<i class="fa fa-music"style="font-size:24px;cursor:hand; padding-left:12px;color:rgba(0,0,0,0.8 );"id="audios"value="Click"></i>
	</label>
	 <!-- for video -->
	<label>
         <i class="fa fa-file-video-o"style="font-size:24px;cursor:hand; padding-left:12px;color:rgba(0,0,0,0.8 );" id="videos"value="Click"title='Add video'></i>
    </label>
	 <!-- for location -->
	<label>
		 <i class="fa fa-map-marker"style="font-size:24px;cursor:hand; padding-left:12px;color:rgba(0,0,0,0.8 );"  id="loc"value="Click"title='Set Location'></i>
    </label>
	 <!-- for poll -->
	<label>
		 <i class="fa fa-plus-square"style="font-size:24px;cursor:hand; padding-left:12px;color:rgba(0,0,0,0.8 );"title='Create poll'data-toggle="modal" data-target="#poll"/></i>
    </label>
       <!-- Modal -->
	 <div id="poll" class="modal fade" role="dialog">
	   <div class="modal-dialog">
	 <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	       <button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Create Poll</h4>
	     </div>
		<div class="modal-body">
		  <form action="post.php"method="post">
	         <div class="form-group">
				<label for="q">Enter question here:</label>
				<input type="text" class="form-control" name='q'id='q'/>
				   <label for="o1">Enter option 1:</label>
				  <input type="text" class="form-control" name='o1'id='o1'/>
					<label for="o2">Enter option 2:</label>
				  <input type="text" class="form-control" name='o2'id='o2'/>
					<label for="o3">Enter option 3:</label>
				  <input type="text" class="form-control" name='o3'id='o3'/>
					<label for="o4">Enter option 4:</label>
				  <input type="text" class="form-control" name="o4"id="o4"/>
             </div>
	   </div>
	    <div class="modal-footer">
			<input type="submit"name="poll"value="Upload"class="btn btn-default"/>
		 </form>
	            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	     </div> 
		</div>
	   </div>
	</div>
	<label>
		 <!-- for file -->
		<i class="fa fa-file-archive-o"style="font-size:24px;cursor:hand; padding-left:12px;color:rgba(0,0,0,0.8 );" id="files"value="Click"title='Add file'></i>
    </label>
	 <!-- for privacy -->
    <label>
			<i class="fa fa-lock"style="cursor:hand;"  id="privacysa"value="Click"title='Set Privacy'></i>
    </label>
	      <button name='post'type='submit'class='btm btn-primary'style='float:right;margin-top:4px;'>Post</button>
   </div>
 </div>
</div>
</form>
	 <script src='script/scripts.js'></script>