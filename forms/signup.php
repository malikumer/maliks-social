
<title><?php echo $malik['title:signup']; ?></title>
<h3 class="text-center"><?php echo $malik['sign:menu']; ?></h3>
	<?php if(!empty($errors)): ?>
		<div class="alert alert-danger">
			<ul>
				<?php foreach ($errors as $value) : ?>
					<li><?= $value ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif;?>
	<?php if ($created): ?>
		<div class="alert alert-success"><?php echo $malik['success:signup']; ?></div>
		<?php $created = false; ?>
	<?php endif ?>
	<form action="" method="post">
	<center><div class="form-group col-sm-12">
				<label for="fname"><?php echo $malik['fname:signup']; ?></label><br />
				<?= input("fname", "text") ?>
			</div>
				<div class="form-group col-sm-12">
				<label for="lname"><?php echo $malik['lname:signup']; ?></label><br />
				<?= input("lname", "text") ?>
			</div>
			<div class="form-group col-sm-12">
				<label for="username"><?php echo $malik['username:signup']; ?></label><br />
				<?= input("username", "text") ?>
			</div>
			<div class="form-group col-sm-12">
				<label for="email"><?php echo $malik['email:signup']; ?></label><br />
				<?= input("email", "text") ?>
			</div>
			<div class="form-group col-sm-12">
				<label for="password"><?php echo $malik['password:signup']; ?></label><br />
				<?= input("password", "password") ?>
			</div>
			<div class="form-group col-sm-12">
				<label for="confirm"><?php echo $malik['cpassword:signup']; ?></label><br />
				<?= input("confirm", "password") ?>
			</div>
			<div class="form-group col-sm-12">
				<input type='text'name='ip'value='<?php echo $ip->malik_get_ip(); ?>'hidden />
			</div>
			<?php if(malik_get_setting_by_value()['capcha_on_of'] ==='On' ){ ?>
			<img src='forms/capcha.php'><br><br>
			<label for="confirm"><?php echo $malik['capcha:signup']; ?></label><br />
			<input type='text'name='capcha'/>
			<br><br>
			<?php } ?>
			<div class="form-group col-sm-12">
				<button value="submit" type="submit" class="btn btn-default"><?php echo $malik['sign:menu']; ?></button>
			</div></center>
	</form>
