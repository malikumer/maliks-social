    <script>

        // an XMLHttpRequest
        var xhr = null;

        /*
         * void
         * quote()
         *
         * Gets a quote.
         */
        function signup()
        {
            // instantiate XMLHttpRequest object
            try
            {
                xhr = new XMLHttpRequest();
            }
            catch (e)
            {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }

            // handle old browsers
            if (xhr == null)
            {
                alert("Ajax not supported by your browser!");
                return;
            }
    var fname = document.getElementById("fname").value;
	var lname = document.getElementById("lname").value;
    var username = document.getElementById("username").value;
	var email = document.getElementById("email").value;
    var username = document.getElementById("username").value;
	var password = document.getElementById("password").value;
    var confirm = document.getElementById("confirm").value;
            // construct URL
            var url = "signup.php?fname=" + fname + "&lname=" + lname + "&email=" + email + "&username=" + username + "&password=" + password + "&confirm=" + confirm + 
            // get quote
            xhr.onreadystatechange = handler;
            xhr.open("GET", url, true);
            xhr.send(null);
        }


        /*
         * void
         * handler()
         *
         * Handles the Ajax response.
         */
        function handler()
        {
            // only handle loaded requests
            if (xhr.readyState == 4)
            {
                if (xhr.status == 200)
                     document.getElementById("result").innerHTML=("record updated successfully");
                else
                    alert("Error with Ajax call!");
            }
        }

    </script>

<h3 class="text-center">Sign up</h3>
	<?php if(!empty($errors)): ?>
		<div class="alert alert-danger">
			<ul>
				<?php foreach ($errors as $value) : ?>
					<li><?= $value ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif;?>
	<?php if ($created): ?>
		<div class="alert alert-success">Your account has been registered! We have sent you an account activation email. If you didn't receive the email, please check your spam/junk folder</div>
		<?php $created = false; ?>
	<?php endif ?>
	<form action=""  method="get" onsubmit="signup(); return false;">
	<center><div class="form-group col-sm-12">
				<label for="fname">First name</label><br />
				 <input id="fname" name="fname" type="text">
			</div>
				<div class="form-group col-sm-12">
				<label for="lname">Last name</label><br />
				<input id="lname" name="lname" type="text">
			</div>
			<div class="form-group col-sm-12">
				<label for="username">Username</label><br />
				<input id="username" name="username" type="text">
			</div>
			<div class="form-group col-sm-12">
				<label for="email">email</label><br />
				<input id="email" name="email" type="text">
			</div>
			<div class="form-group col-sm-12">
				<label for="password">password</label><br />
				<input id="password" name="password" type="password">
			</div>
			<div class="form-group col-sm-12">
				<label for="confirm">confirm password</label><br /><input id="confirm" name="confirm" type="password">
			</div>
			<div class="form-group col-sm-12">
				<button value="submit" type="submit" class="btn btn-default">Sign up</button>
			</div></center>
	</form>
