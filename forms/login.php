<title><?php echo $malik['title:login']; ?></title>
<h3 class="text-center"> <?php echo $malik['login:menu']; ?></h3>
 <?php if ($connected === false): ?>
		<div class="alert alert-danger">
			 <?php echo $malik['error:login']; ?>
		</div>
		<?php $connected = null; ?>
	<?php endif; ?>

	<div class="signin-form">

	<div class="container">
     <form class="form-signin" method="post" id="login-form">
			<div class="form-group col-sm-12">
				<label for="username"> <?php echo $malik['uname:login']; ?></label>
		        <?= input("username", "text","width:40%;height:35px;") ?>
			</div>
			<div class="form-group col-sm-12">
				<label for="password">  <?php echo $malik['password:login']; ?></label>
				<?= input("password", "password","width:40%;height:35px;") ?>
			</div>
			<div class="form-group col-sm-12">
				<label for="remind"></label>
				<input type='checkbox'name='remind'> <label for="remind">  <?php echo $malik['keeplogin:login']; ?></label>
			</div>
		   <div class="form-group">
            <button type="submit" class="btn btn-default" >
    		<span class="glyphicon glyphicon-log-in"></span> &nbsp;  <?php echo $malik['sign:login']; ?>
			</button> 
          </div>  
	</form>