<?php
require_once "includes/includes.php";
     $salts =  $_SESSION['login_user'];
	 $session_user_id = malik_get_users_by_salts($salts)['id'];
    $username = malik_get_users_by_id($session_user_id)['username'];
     $session->malik_is_checklogin();
	if($get_id = $_GET['id']){
	$id = $get_id;
	$create = $db->prepare("DELETE FROM users where id=?");
	$create->execute(array($id));
	header('Location:logout.php?info=Your account has been  deleted successfully');
	}
	if($post_id = $_GET['post_id']){
	$id_post = $post_id;
	$username = $_GET['username'];
	$user_id = $_GET['user_id'];
	$showpost = $db->prepare("select * from post where id=$id_post");
	$showpost->execute();
	$row = $showpost->fetchObject();
	$image = $row->image;
	$video = $row->video;
	$audio = $row->audio;
	$file = $row->file;
	$image_loc = "userdata/users/$user_id/post/$image";
	$video_loc = "userdata/users/$user_id/video/$video";
	$audio_loc = "userdata/users/$user_id/audio/$audio";
	$file_loc = "userdata/users/$user_id/file/$file";
	if(file_exists($image_loc)){
		unlink($image_loc);
	}
	if(file_exists($video_loc)){
		unlink($video_loc);
	}
	if(file_exists($audio_loc)){
		unlink($audio_loc);
	}
	if(file_exists($file_loc)){
		unlink($file_loc);
	}
	$creates = $db->prepare("DELETE FROM post where id=?");
	$creates->execute(array($id_post));
	header("Location:profile.php?username=$username&pages=timeline&info=Your Post has been  deleted successfully");
	}
if($poll_id = $_GET['poll_id']){
    $poll_id = $_GET['poll_id'];
	$creates = $db->prepare("DELETE FROM poll where id=?");
	$creates->execute(array($poll_id));
	header("Location:profile.php?username=$username&pages=timeline&info=Your poll has been  deleted successfully");	
}	